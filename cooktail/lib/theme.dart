import 'package:flutter/material.dart';


final Color primarybgcolor = Color.fromRGBO(58, 58, 58, 1.0);
final Color secondarybgcolor = Color.fromRGBO(30, 29, 30, 1);

// Other theme properties...



final ThemeData appTheme = ThemeData(
  backgroundColor: primarybgcolor,
  cardColor: secondarybgcolor,
  // Add more theme properties as needed
);


final TextStyle titleTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 25,
  fontWeight: FontWeight.bold,
);

final TextStyle bodyTextStyle = TextStyle(
  color: Colors.white70,
  fontSize: 16,
  fontWeight: FontWeight.w500,
);

