// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredient_repository.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Ingredient_Repository _$Ingredient_RepositoryFromJson(
        Map<String, dynamic> json) =>
    $checkedCreate(
      'Ingredient_Repository',
      json,
      ($checkedConvert) {
        final val = Ingredient_Repository(
          idIngredient: $checkedConvert('idIngredient', (v) => v as String?),
          strIngredient: $checkedConvert('strIngredient', (v) => v as String?),
          strDescription:
              $checkedConvert('strDescription', (v) => v as String?),
          strType: $checkedConvert('strType', (v) => v as String?),
          strAlcohol: $checkedConvert('strAlcohol', (v) => v as String?),
          strABV: $checkedConvert('strABV', (v) => v as String?),
        );
        return val;
      },
    );

Map<String, dynamic> _$Ingredient_RepositoryToJson(
        Ingredient_Repository instance) =>
    <String, dynamic>{
      'idIngredient': instance.idIngredient,
      'strIngredient': instance.strIngredient,
      'strDescription': instance.strDescription,
      'strType': instance.strType,
      'strAlcohol': instance.strAlcohol,
      'strABV': instance.strABV,
    };
