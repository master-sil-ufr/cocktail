import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';

part 'ingredient_repository.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Ingredient_Repository extends Equatable {
  const Ingredient_Repository({
    required this.idIngredient,
    required this.strIngredient,
    required this.strDescription,
    required this.strType,
    required this.strAlcohol,
    required this.strABV,
  });

  factory Ingredient_Repository.fromJson(Map<String, dynamic> json) =>
      _$Ingredient_RepositoryFromJson(json);

  Map<String, dynamic> toJson() => _$Ingredient_RepositoryToJson(this);

  final String? idIngredient;
  final String? strIngredient;
  final String? strDescription;
  final String? strType;
  final String? strAlcohol;
  final String? strABV;

  @override
  List<Object?> get props => [ idIngredient,strIngredient,strDescription, strType, strAlcohol, strABV];

}