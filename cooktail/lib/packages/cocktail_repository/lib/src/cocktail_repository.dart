import 'dart:async';
import 'package:the_cocktail_api/the_cocktail_api.dart';
import 'package:cocktail_repository/cocktail_repository.dart';

class CocktailRepository {
  CocktailRepository({TheCocktailApiClient? theCocktailApiClient})
      : _theCocktailApiClient = theCocktailApiClient ?? TheCocktailApiClient();

  final TheCocktailApiClient _theCocktailApiClient;

  Future<Cocktail_Repository> getRandomCocktail() async {
    final cocktail = await _theCocktailApiClient.getRandomCocktail();

    return Cocktail_Repository(
       idDrink : cocktail.idDrink,
       strDrink : cocktail.strDrink,
       strDrinkAlternate : cocktail.strDrinkAlternate,
       strTags : cocktail.strTags,
       strVideo : cocktail.strVideo,
       strCategory : cocktail.strCategory,
       strIBA : cocktail.strIBA,
       strAlcoholic : cocktail.strAlcoholic,
       strGlass : cocktail.strGlass,
       strInstructions : cocktail.strInstructions,
       strInstructionsES : cocktail.strInstructionsES,
       strInstructionsDE : cocktail.strInstructionsDE,
       strInstructionsFR : cocktail.strInstructionsFR,
       strInstructionsIT : cocktail.strInstructionsIT,
       strInstructionsZH_HANS : cocktail.strInstructionsZH_HANS,
       strInstructionsZH_HANT : cocktail.strInstructionsZH_HANT,
       strDrinkThumb : cocktail.strDrinkThumb,
       strIngredient1 : cocktail.strIngredient1,
       strIngredient2 : cocktail.strIngredient2,
       strIngredient3 : cocktail.strIngredient3,
       strIngredient4 : cocktail.strIngredient4,
       strIngredient5 : cocktail.strIngredient5,
       strIngredient6 : cocktail.strIngredient6,
       strIngredient7 : cocktail.strIngredient7,
       strIngredient8 : cocktail.strIngredient8,
       strIngredient9 : cocktail.strIngredient9,
       strIngredient10 : cocktail.strIngredient10,
       strIngredient11 : cocktail.strIngredient11,
       strIngredient12 : cocktail.strIngredient12,
       strIngredient13 : cocktail.strIngredient13,
       strIngredient14 : cocktail.strIngredient14,
       strIngredient15 : cocktail.strIngredient15,
       strMeasure1 : cocktail.strMeasure1,
       strMeasure2 : cocktail.strMeasure2,
       strMeasure3 : cocktail.strMeasure3,
       strMeasure4 : cocktail.strMeasure4,
       strMeasure5 : cocktail.strMeasure5,
       strMeasure6 : cocktail.strMeasure6,
       strMeasure7 : cocktail.strMeasure7,
       strMeasure8 : cocktail.strMeasure8,
       strMeasure9 : cocktail.strMeasure9,
       strMeasure10 : cocktail.strMeasure10,
       strMeasure11 : cocktail.strMeasure11,
       strMeasure12 : cocktail.strMeasure12,
       strMeasure13 : cocktail.strMeasure13,
       strMeasure14 : cocktail.strMeasure14,
       strMeasure15 : cocktail.strMeasure15,
       strImageSource : cocktail.strImageSource,
       strImageAttribution : cocktail.strImageAttribution,
       strCreativeCommonsConfirmed : cocktail.strCreativeCommonsConfirmed,
       dateModified : cocktail.dateModified,
    );
  }

  Future<Cocktail_Repository> getCocktailById(String value) async {
     final cocktail = await _theCocktailApiClient.getCocktailById(value);

     return Cocktail_Repository(
        idDrink : cocktail.idDrink,
        strDrink : cocktail.strDrink,
        strDrinkAlternate : cocktail.strDrinkAlternate,
        strTags : cocktail.strTags,
        strVideo : cocktail.strVideo,
        strCategory : cocktail.strCategory,
        strIBA : cocktail.strIBA,
        strAlcoholic : cocktail.strAlcoholic,
        strGlass : cocktail.strGlass,
        strInstructions : cocktail.strInstructions,
        strInstructionsES : cocktail.strInstructionsES,
        strInstructionsDE : cocktail.strInstructionsDE,
        strInstructionsFR : cocktail.strInstructionsFR,
        strInstructionsIT : cocktail.strInstructionsIT,
        strInstructionsZH_HANS : cocktail.strInstructionsZH_HANS,
        strInstructionsZH_HANT : cocktail.strInstructionsZH_HANT,
        strDrinkThumb : cocktail.strDrinkThumb,
        strIngredient1 : cocktail.strIngredient1,
        strIngredient2 : cocktail.strIngredient2,
        strIngredient3 : cocktail.strIngredient3,
        strIngredient4 : cocktail.strIngredient4,
        strIngredient5 : cocktail.strIngredient5,
        strIngredient6 : cocktail.strIngredient6,
        strIngredient7 : cocktail.strIngredient7,
        strIngredient8 : cocktail.strIngredient8,
        strIngredient9 : cocktail.strIngredient9,
        strIngredient10 : cocktail.strIngredient10,
        strIngredient11 : cocktail.strIngredient11,
        strIngredient12 : cocktail.strIngredient12,
        strIngredient13 : cocktail.strIngredient13,
        strIngredient14 : cocktail.strIngredient14,
        strIngredient15 : cocktail.strIngredient15,
        strMeasure1 : cocktail.strMeasure1,
        strMeasure2 : cocktail.strMeasure2,
        strMeasure3 : cocktail.strMeasure3,
        strMeasure4 : cocktail.strMeasure4,
        strMeasure5 : cocktail.strMeasure5,
        strMeasure6 : cocktail.strMeasure6,
        strMeasure7 : cocktail.strMeasure7,
        strMeasure8 : cocktail.strMeasure8,
        strMeasure9 : cocktail.strMeasure9,
        strMeasure10 : cocktail.strMeasure10,
        strMeasure11 : cocktail.strMeasure11,
        strMeasure12 : cocktail.strMeasure12,
        strMeasure13 : cocktail.strMeasure13,
        strMeasure14 : cocktail.strMeasure14,
        strMeasure15 : cocktail.strMeasure15,
        strImageSource : cocktail.strImageSource,
        strImageAttribution : cocktail.strImageAttribution,
        strCreativeCommonsConfirmed : cocktail.strCreativeCommonsConfirmed,
        dateModified : cocktail.dateModified,
     );
  }

  Future<Ingredient_Repository> getIngredientById(String value) async {
     final ingredient = await _theCocktailApiClient.getIngredientById(value);

     return Ingredient_Repository(
         idIngredient: ingredient.idIngredient,
         strIngredient: ingredient.strIngredient,
         strDescription: ingredient.strDescription,
         strType: ingredient.strType,
         strAlcohol: ingredient.strAlcohol,
         strABV: ingredient.strABV
     );
  }

  Future<List<Cocktail_Repository>> getListOfCategories() async {
     final cocktail = await _theCocktailApiClient.getListOfCategories();
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.category(strCategory: c.strCategory));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> getListOfGlasses() async {
     final cocktail = await _theCocktailApiClient.getListOfGlasses();
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.glass(strGlass: c.strGlass));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> getListOfIngredients() async {
     final cocktail = await _theCocktailApiClient.getListOfIngredients();
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.ingredient(strIngredient1: c.strIngredient1));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> getListOfAlcoholics() async {
     final cocktail = await _theCocktailApiClient.getListOfAlcoholic();
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.alcoholic(strAlcoholic: c.strAlcoholic));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> filterByCategorie(String value) async {
     final cocktail = await _theCocktailApiClient.filterByCategorie(value);
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.succinct(strDrink: c.strDrink,strDrinkThumb: c.strDrinkThumb, idDrink: c.idDrink));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> filterByGlass(String value) async {
     final cocktail = await _theCocktailApiClient.filterByGlass(value);
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.succinct(strDrink: c.strDrink,strDrinkThumb: c.strDrinkThumb, idDrink: c.idDrink));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> filterByAlcoholic(String value) async {
     final cocktail = await _theCocktailApiClient.filterByAlcoholic(value);
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.succinct(strDrink: c.strDrink,strDrinkThumb: c.strDrinkThumb, idDrink: c.idDrink));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> filterByIngredient(String value) async {
     final cocktail = await _theCocktailApiClient.filterByIngredient(value);
     List<Cocktail_Repository> res=[];
     for(Cocktail c in cocktail){
        res.add(Cocktail_Repository.succinct(strDrink: c.strDrink,strDrinkThumb: c.strDrinkThumb, idDrink: c.idDrink));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> searchCocktailByName(String value) async{
     final c = await _theCocktailApiClient.searchCocktailByName(value);
     List<Cocktail_Repository> res=[];
     for(Cocktail cocktail in c){
        res.add(Cocktail_Repository(
           idDrink : cocktail.idDrink,
           strDrink : cocktail.strDrink,
           strDrinkAlternate : cocktail.strDrinkAlternate,
           strTags : cocktail.strTags,
           strVideo : cocktail.strVideo,
           strCategory : cocktail.strCategory,
           strIBA : cocktail.strIBA,
           strAlcoholic : cocktail.strAlcoholic,
           strGlass : cocktail.strGlass,
           strInstructions : cocktail.strInstructions,
           strInstructionsES : cocktail.strInstructionsES,
           strInstructionsDE : cocktail.strInstructionsDE,
           strInstructionsFR : cocktail.strInstructionsFR,
           strInstructionsIT : cocktail.strInstructionsIT,
           strInstructionsZH_HANS : cocktail.strInstructionsZH_HANS,
           strInstructionsZH_HANT : cocktail.strInstructionsZH_HANT,
           strDrinkThumb : cocktail.strDrinkThumb,
           strIngredient1 : cocktail.strIngredient1,
           strIngredient2 : cocktail.strIngredient2,
           strIngredient3 : cocktail.strIngredient3,
           strIngredient4 : cocktail.strIngredient4,
           strIngredient5 : cocktail.strIngredient5,
           strIngredient6 : cocktail.strIngredient6,
           strIngredient7 : cocktail.strIngredient7,
           strIngredient8 : cocktail.strIngredient8,
           strIngredient9 : cocktail.strIngredient9,
           strIngredient10 : cocktail.strIngredient10,
           strIngredient11 : cocktail.strIngredient11,
           strIngredient12 : cocktail.strIngredient12,
           strIngredient13 : cocktail.strIngredient13,
           strIngredient14 : cocktail.strIngredient14,
           strIngredient15 : cocktail.strIngredient15,
           strMeasure1 : cocktail.strMeasure1,
           strMeasure2 : cocktail.strMeasure2,
           strMeasure3 : cocktail.strMeasure3,
           strMeasure4 : cocktail.strMeasure4,
           strMeasure5 : cocktail.strMeasure5,
           strMeasure6 : cocktail.strMeasure6,
           strMeasure7 : cocktail.strMeasure7,
           strMeasure8 : cocktail.strMeasure8,
           strMeasure9 : cocktail.strMeasure9,
           strMeasure10 : cocktail.strMeasure10,
           strMeasure11 : cocktail.strMeasure11,
           strMeasure12 : cocktail.strMeasure12,
           strMeasure13 : cocktail.strMeasure13,
           strMeasure14 : cocktail.strMeasure14,
           strMeasure15 : cocktail.strMeasure15,
           strImageSource : cocktail.strImageSource,
           strImageAttribution : cocktail.strImageAttribution,
           strCreativeCommonsConfirmed : cocktail.strCreativeCommonsConfirmed,
           dateModified : cocktail.dateModified,
        ));
     }
     return res;
  }

  Future<List<Cocktail_Repository>> listCocktailByFirstLetter(String value) async{
     final c = await _theCocktailApiClient.listCocktailByFirstLetter(value);
     List<Cocktail_Repository> res=[];
     for(Cocktail cocktail in c){
        res.add(Cocktail_Repository(
           idDrink : cocktail.idDrink,
           strDrink : cocktail.strDrink,
           strDrinkAlternate : cocktail.strDrinkAlternate,
           strTags : cocktail.strTags,
           strVideo : cocktail.strVideo,
           strCategory : cocktail.strCategory,
           strIBA : cocktail.strIBA,
           strAlcoholic : cocktail.strAlcoholic,
           strGlass : cocktail.strGlass,
           strInstructions : cocktail.strInstructions,
           strInstructionsES : cocktail.strInstructionsES,
           strInstructionsDE : cocktail.strInstructionsDE,
           strInstructionsFR : cocktail.strInstructionsFR,
           strInstructionsIT : cocktail.strInstructionsIT,
           strInstructionsZH_HANS : cocktail.strInstructionsZH_HANS,
           strInstructionsZH_HANT : cocktail.strInstructionsZH_HANT,
           strDrinkThumb : cocktail.strDrinkThumb,
           strIngredient1 : cocktail.strIngredient1,
           strIngredient2 : cocktail.strIngredient2,
           strIngredient3 : cocktail.strIngredient3,
           strIngredient4 : cocktail.strIngredient4,
           strIngredient5 : cocktail.strIngredient5,
           strIngredient6 : cocktail.strIngredient6,
           strIngredient7 : cocktail.strIngredient7,
           strIngredient8 : cocktail.strIngredient8,
           strIngredient9 : cocktail.strIngredient9,
           strIngredient10 : cocktail.strIngredient10,
           strIngredient11 : cocktail.strIngredient11,
           strIngredient12 : cocktail.strIngredient12,
           strIngredient13 : cocktail.strIngredient13,
           strIngredient14 : cocktail.strIngredient14,
           strIngredient15 : cocktail.strIngredient15,
           strMeasure1 : cocktail.strMeasure1,
           strMeasure2 : cocktail.strMeasure2,
           strMeasure3 : cocktail.strMeasure3,
           strMeasure4 : cocktail.strMeasure4,
           strMeasure5 : cocktail.strMeasure5,
           strMeasure6 : cocktail.strMeasure6,
           strMeasure7 : cocktail.strMeasure7,
           strMeasure8 : cocktail.strMeasure8,
           strMeasure9 : cocktail.strMeasure9,
           strMeasure10 : cocktail.strMeasure10,
           strMeasure11 : cocktail.strMeasure11,
           strMeasure12 : cocktail.strMeasure12,
           strMeasure13 : cocktail.strMeasure13,
           strMeasure14 : cocktail.strMeasure14,
           strMeasure15 : cocktail.strMeasure15,
           strImageSource : cocktail.strImageSource,
           strImageAttribution : cocktail.strImageAttribution,
           strCreativeCommonsConfirmed : cocktail.strCreativeCommonsConfirmed,
           dateModified : cocktail.dateModified,
        ));
     }
     return res;
  }

  Future<Ingredient_Repository> searchIngredientByName(String value) async {
     final ingredient = await _theCocktailApiClient.searchIngredientByName(value);

     return Ingredient_Repository(
         idIngredient: ingredient.idIngredient,
         strIngredient: ingredient.strIngredient,
         strDescription: ingredient.strDescription,
         strType: ingredient.strType,
         strAlcohol: ingredient.strAlcohol,
         strABV: ingredient.strABV
     );
  }
}

