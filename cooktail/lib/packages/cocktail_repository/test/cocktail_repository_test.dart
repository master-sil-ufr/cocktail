// ignore_for_file: prefer_const_constructors
import 'package:mocktail/mocktail.dart';
import 'package:the_cocktail_api/the_cocktail_api.dart' as the_cocktail_api;
import 'package:test/test.dart';
import 'package:cocktail_repository/cocktail_repository.dart';

class MockTheCocktailApiClient extends Mock
    implements the_cocktail_api.TheCocktailApiClient {}

class MockCocktail extends Mock implements the_cocktail_api.Cocktail {}

void main() {
  group('CocktailRepository', () {
    late the_cocktail_api.TheCocktailApiClient cocktailApiClient;
    late CocktailRepository cocktailRepository;

    setUp(() {
      cocktailApiClient = MockTheCocktailApiClient();
      cocktailRepository = CocktailRepository(
        theCocktailApiClient: cocktailApiClient,
      );
    });

    group('constructor', () {
      test('instantiates internal cocktail api client when not injected', () {
        expect(CocktailRepository(), isNotNull);
      });
    });

    group('getRandomCocktail', () {
      test('calls getRandomCocktail', () async {
        try {
          await cocktailRepository.getRandomCocktail();
        } catch (_) {}
        verify(() => cocktailApiClient.getRandomCocktail()).called(1);
      });

      test('throws when getRandomCocktail fails', () async {
        final exception = Exception('oops');
        when(() => cocktailApiClient.getRandomCocktail()).thenThrow(exception);
        expect(
              () async => cocktailRepository.getRandomCocktail(),
          throwsA(exception),
        );
      });

      test('throws when getRandomCocktail fails', () async {
        final exception = Exception('oops');
        when(
              () => cocktailApiClient.getRandomCocktail(),
        ).thenThrow(exception);
        expect(
              () async => cocktailRepository.getRandomCocktail(),
          throwsA(exception),
        );
      });
    });
  });
}
