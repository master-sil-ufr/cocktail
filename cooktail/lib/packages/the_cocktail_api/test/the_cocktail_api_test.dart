import 'package:the_cocktail_api/the_cocktail_api.dart';
import 'package:test/test.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';

class MockHttpClient extends Mock implements http.Client {}

class MockResponse extends Mock implements http.Response {}

class FakeUri extends Fake implements Uri {}

void main() {
  group('TheCocktailApiClient', () {
    late http.Client httpClient;
    late TheCocktailApiClient apiClient;

    setUpAll(() {
      registerFallbackValue(FakeUri());
    });

    setUp(() {
      httpClient = MockHttpClient();
      apiClient = TheCocktailApiClient(httpClient: httpClient);
    });

    group('constructor', () {
      test('does not require an httpClient', () {
        expect(TheCocktailApiClient(), isNotNull);
      });
    });

    group('randomCocktailSearch', () {
      test('makes correct http request', () async {
        final response = MockResponse();
        when(() => response.statusCode).thenReturn(200);
        when(() => response.body).thenReturn('{}');
        when(() => httpClient.get(any())).thenAnswer((_) async => response);
        try {
          await apiClient.getRandomCocktail();
        } catch (_) {}
        verify(
              () => httpClient.get(
            Uri.https(
              'www.thecocktaildb.com',
              '/api/json/v1/1/random.php',
            ),
          ),
        ).called(1);
      });

      test('throws RandomCocktailRequestFailure on non-200 response', () async {
        final response = MockResponse();
        when(() => response.statusCode).thenReturn(400);
        when(() => httpClient.get(any())).thenAnswer((_) async => response);
        expect(
              () async => apiClient.getRandomCocktail(),
          throwsA(isA<RandomCocktailRequestFailure>()),
        );
      });

      test('throws RandomCocktailNotFoundFailure on error response', () async {
        final response = MockResponse();
        when(() => response.statusCode).thenReturn(200);
        when(() => response.body).thenReturn('{}');
        when(() => httpClient.get(any())).thenAnswer((_) async => response);
        await expectLater(
          apiClient.getRandomCocktail(),
          throwsA(isA<RandomCocktailNotFoundFailure>()),
        );
      });

      test('throws RandomCocktailNotFoundFailure on empty response', () async {
        final response = MockResponse();
        when(() => response.statusCode).thenReturn(200);
        when(() => response.body).thenReturn('{"drinks": []}');
        when(() => httpClient.get(any())).thenAnswer((_) async => response);
        await expectLater(
          apiClient.getRandomCocktail(),
          throwsA(isA<RandomCocktailNotFoundFailure>()),
        );
      });
    });
  });
}