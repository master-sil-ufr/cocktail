import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:the_cocktail_api/the_cocktail_api.dart';

/// Exception thrown when random cocktail fails.
class RandomCocktailRequestFailure implements Exception {}
/// Exception thrown when the provided random cocktail is not found.
class RandomCocktailNotFoundFailure implements Exception {}

/// Exception thrown when cocktail by id fails.
class CocktailByIdRequestFailure implements Exception {}
/// Exception thrown when the provided cocktail by id is not found.
class CocktailByIdNotFoundFailure implements Exception {}

/// Exception thrown when ingredient by id fails.
class IngredientByIdRequestFailure implements Exception {}
/// Exception thrown when the provided ingredient by id is not found.
class IngredientByIdNotFoundFailure implements Exception {}

/// Exception thrown when list Categories fails.
class ListCategoriesRequestFailure implements Exception {}
/// Exception thrown when the provided list Categories is not found.
class ListCategoriesNotFoundFailure implements Exception {}

/// Exception thrown when list Glasses fails.
class ListGlassesRequestFailure implements Exception {}
/// Exception thrown when the provided list Glasses is not found.
class ListGlassesNotFoundFailure implements Exception {}

/// Exception thrown when list Ingredients fails.
class ListIngredientsRequestFailure implements Exception {}
/// Exception thrown when the provided list Ingredients is not found.
class ListIngredientsNotFoundFailure implements Exception {}

/// Exception thrown when list Alcoholic fails.
class ListAlcoholicRequestFailure implements Exception {}
/// Exception thrown when the provided list Alcoholic is not found.
class ListAlcoholicNotFoundFailure implements Exception {}

/// Exception thrown when Filter By Categorie fails.
class FilterByCategorieRequestFailure implements Exception {}
/// Exception thrown when the provided Filter By Categorie is not found.
class FilterByCategorieNotFoundFailure implements Exception {}

/// Exception thrown when Filter By Glass fails.
class FilterByGlassRequestFailure implements Exception {}
/// Exception thrown when the provided Filter By Glass is not found.
class FilterByGlassNotFoundFailure implements Exception {}

/// Exception thrown when Filter By Ingredient fails.
class FilterByIngredientRequestFailure implements Exception {}
/// Exception thrown when the provided Filter By Ingredient is not found.
class FilterByIngredientNotFoundFailure implements Exception {}

/// Exception thrown when Filter By Alcoholic fails.
class FilterByAlcoholicRequestFailure implements Exception {}
/// Exception thrown when the provided Filter By Alcoholic is not found.
class FilterByAlcoholicNotFoundFailure implements Exception {}

/// Exception thrown when search cocktail by name fails.
class SearchCocktailByNameRequestFailure implements Exception {}
/// Exception thrown when the provided search cocktail by name is not found.
class SearchCocktailByNameNotFoundFailure implements Exception {}

/// Exception thrown when list cocktail with first letter fails.
class ListCocktailByFirstLetterRequestFailure implements Exception {}
/// Exception thrown when the provided list cocktail with first letter is not found.
class ListCocktailByFirstLetterNotFoundFailure implements Exception {}

/// Exception thrown when list cocktail with first lettersearch ingredient by name fails.
class SearchIgredientByNameRequestFailure implements Exception {}
/// Exception thrown when the provided list cocktail with first lettersearch ingredient by name is not found.
class SearchIgredientByNameNotFoundFailure implements Exception {}

/// {@template the_cocktail_api_client}
/// Dart API Client which wraps the [Cocktail API](https://www.thecocktaildb.com).
/// {@endtemplate}
class TheCocktailApiClient {
  /// {@macro the_cocktail_api_client}
  TheCocktailApiClient({http.Client? httpClient})
      : _httpClient = httpClient ?? http.Client();

  static const _baseUrl = 'www.thecocktaildb.com';

  final http.Client _httpClient;

  /// Finds a Random cocktail.
  Future<Cocktail> getRandomCocktail() async {
    final randomCocktailRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/random.php',
    );

    final randomCocktailResponse = await _httpClient.get(randomCocktailRequest);

    if (randomCocktailResponse.statusCode != 200) {
      throw RandomCocktailRequestFailure();
    }

    final randomCocktailJson = jsonDecode(randomCocktailResponse.body) as Map;

    if (!randomCocktailJson.containsKey('drinks')) throw RandomCocktailNotFoundFailure();

    final results = randomCocktailJson['drinks'] as List;

    if (results.isEmpty) throw RandomCocktailNotFoundFailure();
    return Cocktail.fromJson(results.first as Map<String, dynamic>);
  }

  /// Finds a cocktail by Id.
  Future<Cocktail> getCocktailById(String id) async {
    final cocktailByIdRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/lookup.php',
      {'i': id}
    );

    final cocktailByIdResponse = await _httpClient.get(cocktailByIdRequest);

    if (cocktailByIdResponse.statusCode != 200) {
      throw CocktailByIdRequestFailure();
    }

    final cocktailByIdJson = jsonDecode(cocktailByIdResponse.body) as Map;

    if (!cocktailByIdJson.containsKey('drinks')) throw CocktailByIdNotFoundFailure();

    final results = cocktailByIdJson['drinks'] as List;

    if (results.isEmpty) throw CocktailByIdNotFoundFailure();
    return Cocktail.fromJson(results.first as Map<String, dynamic>);
  }

  /// Finds a cocktail by Id.
  Future<Ingredient> getIngredientById(String id) async {
    final ingredientByIdRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/lookup.php',
      {'iid': id}
    );

    final ingredientByIdResponse = await _httpClient.get(ingredientByIdRequest);

    if (ingredientByIdResponse.statusCode != 200) {
      throw IngredientByIdRequestFailure();
    }

    final ingredientByIdJson = jsonDecode(ingredientByIdResponse.body) as Map;

    if (!ingredientByIdJson.containsKey('ingredients')) throw IngredientByIdNotFoundFailure();

    final results = ingredientByIdJson['ingredients'] as List;

    if (results.isEmpty) throw IngredientByIdNotFoundFailure();
    return Ingredient.fromJson(results.first as Map<String, dynamic>);
  }

  /// List the categories
  Future<List<Cocktail>> getListOfCategories() async {
    final listCategoriesRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/list.php',
      {'c': 'list'}
    );

    final listCategoriesResponse = await _httpClient.get(listCategoriesRequest);

    if (listCategoriesResponse.statusCode != 200) {
      throw ListCategoriesRequestFailure();
    }

    final listCategoriesJson = jsonDecode(listCategoriesResponse.body) as Map;

    if (!listCategoriesJson.containsKey('drinks')) throw ListCategoriesNotFoundFailure();

    final results = listCategoriesJson['drinks'] as List;

    if (results.isEmpty) throw ListCategoriesNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// List the Glasses
  Future<List<Cocktail>> getListOfGlasses() async {
    final listGlassesRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/list.php',
        {'g': 'list'}
    );

    final listGlassesResponse = await _httpClient.get(listGlassesRequest);

    if (listGlassesResponse.statusCode != 200) {
      throw ListGlassesRequestFailure();
    }

    final listGlassesJson = jsonDecode(listGlassesResponse.body) as Map;

    if (!listGlassesJson.containsKey('drinks')) throw ListGlassesNotFoundFailure();

    final results = listGlassesJson['drinks'] as List;

    if (results.isEmpty) throw ListGlassesNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// List the Ingredients
  Future<List<Cocktail>> getListOfIngredients() async {
    final listIngredientsRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/list.php',
        {'i': 'list'}
    );

    final listIngredientsResponse = await _httpClient.get(listIngredientsRequest);

    if (listIngredientsResponse.statusCode != 200) {
      throw ListIngredientsRequestFailure();
    }

    final listIngredientsJson = jsonDecode(listIngredientsResponse.body) as Map;

    if (!listIngredientsJson.containsKey('drinks')) throw ListIngredientsNotFoundFailure();

    final results = listIngredientsJson['drinks'] as List;

    if (results.isEmpty) throw ListIngredientsNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// List the Alcoholic
  Future<List<Cocktail>> getListOfAlcoholic() async {
    final listAlcoholicRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/list.php',
        {'a': 'list'}
    );

    final listAlcoholicResponse = await _httpClient.get(listAlcoholicRequest);

    if (listAlcoholicResponse.statusCode != 200) {
      throw ListAlcoholicRequestFailure();
    }

    final listAlcoholicJson = jsonDecode(listAlcoholicResponse.body) as Map;

    if (!listAlcoholicJson.containsKey('drinks')) throw ListAlcoholicNotFoundFailure();

    final results = listAlcoholicJson['drinks'] as List;

    if (results.isEmpty) throw ListAlcoholicNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// filter by categorie
  Future<List<Cocktail>> filterByCategorie(String value) async {
    final filterByCategorieRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/filter.php',
        {'c': value}
    );

    final filterByCategorieResponse = await _httpClient.get(filterByCategorieRequest);

    if (filterByCategorieResponse.statusCode != 200) {
      throw FilterByCategorieRequestFailure();
    }

    final filterByCategorieJson = jsonDecode(filterByCategorieResponse.body) as Map;

    if (!filterByCategorieJson.containsKey('drinks')) throw FilterByCategorieNotFoundFailure();

    final results = filterByCategorieJson['drinks'] as List;

    if (results.isEmpty) throw FilterByCategorieNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// filter by Glass
  Future<List<Cocktail>> filterByGlass(String value) async {
    final filterByGlassRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/filter.php',
        {'g': value}
    );

    final filterByGlassResponse = await _httpClient.get(filterByGlassRequest);

    if (filterByGlassResponse.statusCode != 200) {
      throw FilterByGlassRequestFailure();
    }

    final filterByGlassJson = jsonDecode(filterByGlassResponse.body) as Map;

    if (!filterByGlassJson.containsKey('drinks')) throw FilterByGlassNotFoundFailure();

    final results = filterByGlassJson['drinks'] as List;

    if (results.isEmpty) throw FilterByGlassNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// filter by Ingredient
  Future<List<Cocktail>> filterByIngredient(String value) async {
    final filterByIngredientRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/filter.php',
        {'i': value}
    );

    final filterByIngredientResponse = await _httpClient.get(filterByIngredientRequest);

    if (filterByIngredientResponse.statusCode != 200) {
      throw FilterByIngredientRequestFailure();
    }

    final filterByIngredientJson = jsonDecode(filterByIngredientResponse.body) as Map;

    if (!filterByIngredientJson.containsKey('drinks')) throw FilterByIngredientNotFoundFailure();

    final results = filterByIngredientJson['drinks'] as List;

    if (results.isEmpty) throw FilterByIngredientNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// filter by Alcoholic
  Future<List<Cocktail>> filterByAlcoholic(String value) async {
    final filterByAlcoholicRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/filter.php',
        {'a': value}
    );

    final filterByAlcoholicResponse = await _httpClient.get(filterByAlcoholicRequest);

    if (filterByAlcoholicResponse.statusCode != 200) {
      throw FilterByAlcoholicRequestFailure();
    }

    final filterByAlcoholicJson = jsonDecode(filterByAlcoholicResponse.body) as Map;

    if (!filterByAlcoholicJson.containsKey('drinks')) throw FilterByAlcoholicNotFoundFailure();

    final results = filterByAlcoholicJson['drinks'] as List;

    if (results.isEmpty) throw FilterByAlcoholicNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// Search cocktail by name
  Future<List<Cocktail>> searchCocktailByName(String value) async {
    final searchCocktailByNameRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/search.php',
        {'s': value}
    );

    final searchCocktailByNameResponse = await _httpClient.get(searchCocktailByNameRequest);

    if (searchCocktailByNameResponse.statusCode != 200) {
      throw SearchCocktailByNameRequestFailure();
    }

    final searchCocktailByNameJson = jsonDecode(searchCocktailByNameResponse.body) as Map;

    if (!searchCocktailByNameJson.containsKey('drinks')) throw SearchCocktailByNameNotFoundFailure();

    final results = searchCocktailByNameJson['drinks'] as List;

    if (results.isEmpty) throw SearchCocktailByNameNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// List all cocktails by first letter
  Future<List<Cocktail>> listCocktailByFirstLetter(String value) async {
    if(value.length!=1) {
      ListCocktailByFirstLetterRequestFailure();
    }

    final listCocktailByFirstLetterRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/search.php',
        {'f': value}
    );

    final listCocktailByFirstLetterResponse = await _httpClient.get(listCocktailByFirstLetterRequest);

    if (listCocktailByFirstLetterResponse.statusCode != 200) {
      throw ListCocktailByFirstLetterRequestFailure();
    }

    final listCocktailByFirstLetterJson = jsonDecode(listCocktailByFirstLetterResponse.body) as Map;

    if (!listCocktailByFirstLetterJson.containsKey('drinks')) throw ListCocktailByFirstLetterNotFoundFailure();

    final results = listCocktailByFirstLetterJson['drinks'] as List;

    if (results.isEmpty) throw ListCocktailByFirstLetterNotFoundFailure();
    List<Cocktail> list = results.map((cocktailJson) => Cocktail.fromJson(cocktailJson as Map<String, dynamic>)).toList();
    return list;
  }

  /// Search ingredient by name
  Future<Ingredient> searchIngredientByName(String value) async {
    final searchIgredientByNameRequest = Uri.https(
      _baseUrl,
      '/api/json/v1/1/search.php',
        {'i': value}
    );

    final searchIgredientByNameResponse = await _httpClient.get(searchIgredientByNameRequest);

    if (searchIgredientByNameResponse.statusCode != 200) {
      throw SearchIgredientByNameRequestFailure();
    }

    final searchIgredientByNameJson = jsonDecode(searchIgredientByNameResponse.body) as Map;

    if (!searchIgredientByNameJson.containsKey('ingredients')) throw SearchIgredientByNameNotFoundFailure();

    final results = searchIgredientByNameJson['ingredients'] as List;

    if (results.isEmpty) throw SearchIgredientByNameNotFoundFailure();
    return Ingredient.fromJson(results.first as Map<String, dynamic>);
  }
}