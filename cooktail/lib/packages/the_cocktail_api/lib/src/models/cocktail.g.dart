// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cocktail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cocktail _$CocktailFromJson(Map<String, dynamic> json) => $checkedCreate(
      'Cocktail',
      json,
      ($checkedConvert) {
        final val = Cocktail(
          idDrink: $checkedConvert('idDrink', (v) => v as String?),
          strDrink: $checkedConvert('strDrink', (v) => v as String?),
          strDrinkAlternate:
              $checkedConvert('strDrinkAlternate', (v) => v as String?),
          strTags: $checkedConvert('strTags', (v) => v as String?),
          strVideo: $checkedConvert('strVideo', (v) => v as String?),
          strCategory: $checkedConvert('strCategory', (v) => v as String?),
          strIBA: $checkedConvert('strIBA', (v) => v as String?),
          strAlcoholic: $checkedConvert('strAlcoholic', (v) => v as String?),
          strGlass: $checkedConvert('strGlass', (v) => v as String?),
          strInstructions:
              $checkedConvert('strInstructions', (v) => v as String?),
          strInstructionsES:
              $checkedConvert('strInstructionsES', (v) => v as String?),
          strInstructionsDE:
              $checkedConvert('strInstructionsDE', (v) => v as String?),
          strInstructionsFR:
              $checkedConvert('strInstructionsFR', (v) => v as String?),
          strInstructionsIT:
              $checkedConvert('strInstructionsIT', (v) => v as String?),
          strInstructionsZH_HANS:
              $checkedConvert('strInstructionsZH_HANS', (v) => v as String?),
          strInstructionsZH_HANT:
              $checkedConvert('strInstructionsZH_HANT', (v) => v as String?),
          strDrinkThumb: $checkedConvert('strDrinkThumb', (v) => v as String?),
          strIngredient1:
              $checkedConvert('strIngredient1', (v) => v as String?),
          strIngredient2:
              $checkedConvert('strIngredient2', (v) => v as String?),
          strIngredient3:
              $checkedConvert('strIngredient3', (v) => v as String?),
          strIngredient4:
              $checkedConvert('strIngredient4', (v) => v as String?),
          strIngredient5:
              $checkedConvert('strIngredient5', (v) => v as String?),
          strIngredient6:
              $checkedConvert('strIngredient6', (v) => v as String?),
          strIngredient7:
              $checkedConvert('strIngredient7', (v) => v as String?),
          strIngredient8:
              $checkedConvert('strIngredient8', (v) => v as String?),
          strIngredient9:
              $checkedConvert('strIngredient9', (v) => v as String?),
          strIngredient10:
              $checkedConvert('strIngredient10', (v) => v as String?),
          strIngredient11:
              $checkedConvert('strIngredient11', (v) => v as String?),
          strIngredient12:
              $checkedConvert('strIngredient12', (v) => v as String?),
          strIngredient13:
              $checkedConvert('strIngredient13', (v) => v as String?),
          strIngredient14:
              $checkedConvert('strIngredient14', (v) => v as String?),
          strIngredient15:
              $checkedConvert('strIngredient15', (v) => v as String?),
          strMeasure1: $checkedConvert('strMeasure1', (v) => v as String?),
          strMeasure2: $checkedConvert('strMeasure2', (v) => v as String?),
          strMeasure3: $checkedConvert('strMeasure3', (v) => v as String?),
          strMeasure4: $checkedConvert('strMeasure4', (v) => v as String?),
          strMeasure5: $checkedConvert('strMeasure5', (v) => v as String?),
          strMeasure6: $checkedConvert('strMeasure6', (v) => v as String?),
          strMeasure7: $checkedConvert('strMeasure7', (v) => v as String?),
          strMeasure8: $checkedConvert('strMeasure8', (v) => v as String?),
          strMeasure9: $checkedConvert('strMeasure9', (v) => v as String?),
          strMeasure10: $checkedConvert('strMeasure10', (v) => v as String?),
          strMeasure11: $checkedConvert('strMeasure11', (v) => v as String?),
          strMeasure12: $checkedConvert('strMeasure12', (v) => v as String?),
          strMeasure13: $checkedConvert('strMeasure13', (v) => v as String?),
          strMeasure14: $checkedConvert('strMeasure14', (v) => v as String?),
          strMeasure15: $checkedConvert('strMeasure15', (v) => v as String?),
          strImageSource:
              $checkedConvert('strImageSource', (v) => v as String?),
          strImageAttribution:
              $checkedConvert('strImageAttribution', (v) => v as String?),
          strCreativeCommonsConfirmed: $checkedConvert(
              'strCreativeCommonsConfirmed', (v) => v as String?),
          dateModified: $checkedConvert('dateModified', (v) => v as String?),
        );
        return val;
      },
    );

Map<String, dynamic> _$CocktailToJson(Cocktail instance) => <String, dynamic>{
      'idDrink': instance.idDrink,
      'strDrink': instance.strDrink,
      'strDrinkAlternate': instance.strDrinkAlternate,
      'strTags': instance.strTags,
      'strVideo': instance.strVideo,
      'strCategory': instance.strCategory,
      'strIBA': instance.strIBA,
      'strAlcoholic': instance.strAlcoholic,
      'strGlass': instance.strGlass,
      'strInstructions': instance.strInstructions,
      'strInstructionsES': instance.strInstructionsES,
      'strInstructionsDE': instance.strInstructionsDE,
      'strInstructionsFR': instance.strInstructionsFR,
      'strInstructionsIT': instance.strInstructionsIT,
      'strInstructionsZH_HANS': instance.strInstructionsZH_HANS,
      'strInstructionsZH_HANT': instance.strInstructionsZH_HANT,
      'strDrinkThumb': instance.strDrinkThumb,
      'strIngredient1': instance.strIngredient1,
      'strIngredient2': instance.strIngredient2,
      'strIngredient3': instance.strIngredient3,
      'strIngredient4': instance.strIngredient4,
      'strIngredient5': instance.strIngredient5,
      'strIngredient6': instance.strIngredient6,
      'strIngredient7': instance.strIngredient7,
      'strIngredient8': instance.strIngredient8,
      'strIngredient9': instance.strIngredient9,
      'strIngredient10': instance.strIngredient10,
      'strIngredient11': instance.strIngredient11,
      'strIngredient12': instance.strIngredient12,
      'strIngredient13': instance.strIngredient13,
      'strIngredient14': instance.strIngredient14,
      'strIngredient15': instance.strIngredient15,
      'strMeasure1': instance.strMeasure1,
      'strMeasure2': instance.strMeasure2,
      'strMeasure3': instance.strMeasure3,
      'strMeasure4': instance.strMeasure4,
      'strMeasure5': instance.strMeasure5,
      'strMeasure6': instance.strMeasure6,
      'strMeasure7': instance.strMeasure7,
      'strMeasure8': instance.strMeasure8,
      'strMeasure9': instance.strMeasure9,
      'strMeasure10': instance.strMeasure10,
      'strMeasure11': instance.strMeasure11,
      'strMeasure12': instance.strMeasure12,
      'strMeasure13': instance.strMeasure13,
      'strMeasure14': instance.strMeasure14,
      'strMeasure15': instance.strMeasure15,
      'strImageSource': instance.strImageSource,
      'strImageAttribution': instance.strImageAttribution,
      'strCreativeCommonsConfirmed': instance.strCreativeCommonsConfirmed,
      'dateModified': instance.dateModified,
    };
