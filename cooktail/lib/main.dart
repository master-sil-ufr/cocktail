import 'package:flutter/material.dart';
import 'package:cooktail/view/navbar.dart';
import 'package:cooktail/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en'), // English
        Locale('es'), // Spain
        Locale('fr'), // France
        Locale('it'), // Italia
        Locale('de'), // Deutsch
      ],
      title: 'CookTail',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: appTheme.backgroundColor,
          fontFamily: 'Muli',
          //brightness: Brightness.light,
          appBarTheme: const AppBarTheme(
            color: Colors.white,
            elevation: 0,
            centerTitle: true,
            iconTheme: IconThemeData(color: Color(0XFF8B8B8B)),
            titleTextStyle: TextStyle(color: Color(0XFF8B8B8B), fontSize: 18),
          )
      ),
      home: const Navigation(),
    );
  }
}