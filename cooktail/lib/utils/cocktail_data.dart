import 'package:shared_preferences/shared_preferences.dart';

Future<void> saveDataList(String nameList,List<String> cocktailList) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setStringList(nameList, cocktailList);
}

Future<List<String>> getDataList(String nameList) async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getStringList(nameList) ?? [];
}

Future<int> getDataListSize(String nameList) async {
  final prefs = await SharedPreferences.getInstance();
  List<String>? cocktailList = prefs.getStringList(nameList);
  return cocktailList?.length ?? 0;
}

Future<void> addDataToList(String nameList,String newCocktail) async {
  List<String> currentCocktailList = await getDataList(nameList);
  if(newCocktail.isNotEmpty)
    currentCocktailList.insert(0,newCocktail);
  await saveDataList(nameList,currentCocktailList);
}

Future<void> addDataToListRetreint(String nameList,String newCocktail) async {
  List<String> currentCocktailList = await getDataList(nameList);
  if(newCocktail.isNotEmpty)
    currentCocktailList.insert(0,newCocktail);
  if(currentCocktailList.length>7)
    currentCocktailList.removeLast();
  await saveDataList(nameList,currentCocktailList);
}

Future<void> addOrRemoveDataToList(String nameList,String newCocktail) async {
  List<String> currentCocktailList = await getDataList(nameList);
  if(currentCocktailList.contains(newCocktail))
    currentCocktailList.remove(newCocktail);
  else
    if(newCocktail.isNotEmpty)
      currentCocktailList.insert(0,newCocktail);
  await saveDataList(nameList,currentCocktailList);
}

Future<void> removeAndAddDataToList(String nameList,String newCocktail) async {
  List<String> currentCocktailList = await getDataList(nameList);
  if(currentCocktailList.contains(newCocktail))
    currentCocktailList.remove(newCocktail);
  await saveDataList(nameList,currentCocktailList);
  addDataToListRetreint(nameList,newCocktail);
}

Future<bool> containsDataList(String nameList,String id) async {
  List<String> currentCocktailList = await getDataList(nameList);
  return currentCocktailList.contains(id);
}