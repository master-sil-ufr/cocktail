import 'package:cooktail/theme.dart';
import 'package:cooktail/view/detailcocktail.dart';
import 'package:cooktail/models/models.dart';
import 'package:cocktail_repository/cocktail_repository.dart';
import 'package:flutter/material.dart';
import 'package:cooktail/utils/cocktail_data.dart';
import 'package:cooktail/view/widget/favoriteIcon.dart';

enum ViewType{
  random,
  normal,
}

class FullWeightCocktail extends StatefulWidget {
  final Cocktail? cocktail;
  const FullWeightCocktail({super.key,this.cocktail});

  @override
  State<FullWeightCocktail> createState() => _FullWeightCocktailState();
}

class _FullWeightCocktailState extends State<FullWeightCocktail> {
  late Cocktail c=Cocktail.empty;
  late ViewType vt;
  FutureBuilder<Icon> fi=getFavoriteIcon("");


  void loadRandom() async{
    Cocktail_Repository cr = await CocktailRepository().getRandomCocktail();
    setState(() {
      c = Cocktail.fromRepository(cr);
      fi=getFavoriteIcon(c.idDrink ?? "");
    });
  }

  void loadCocktail() async{
    Cocktail_Repository cr = await CocktailRepository().getRandomCocktail();
    setState(() {
      c = Cocktail.fromRepository(cr);
      fi=getFavoriteIcon(c.idDrink ?? "");
    });
  }

  @override
  void initState(){
    super.initState();
    if(widget.cocktail==null) {
      vt=ViewType.random;
      loadRandom();
    }
    else{
      vt=ViewType.normal;
      c=widget.cocktail!;
      setState(() {
        fi=getFavoriteIcon(c.idDrink ?? "");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    switch(vt){
      case ViewType.random:
        return GestureDetector(
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => Detailcocktail.cocktail(cocktail: c)));
          },
          child: Container(
            height: MediaQuery.of(context).size.height/5,
            decoration: BoxDecoration(
              color: appTheme.cardColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 4,
                  child: SizedBox(
                    height: double.infinity,
                    child:ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                      ),
                      child:
                      Image.network(
                        '${c.strDrinkThumb ?? ""}/preview',
                        fit: BoxFit.cover,
                        errorBuilder:
                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return const Center(
                            child: CircularProgressIndicator(value: 1,),
                          );
                        },
                        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              c.strDrink ?? "",
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              c.getStrIngredient(),
                              overflow: TextOverflow.fade,
                              maxLines: 2,
                              softWrap: true,
                              style: const TextStyle(
                                color: Colors.white70,
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                await addOrRemoveDataToList('Cocktail',c.idDrink ?? "");
                                setState(() {
                                  fi=getFavoriteIcon(c.idDrink ?? "");
                                });
                              },
                              child:Container(
                                color: appTheme.cardColor,
                                padding: const EdgeInsets.fromLTRB(3, 3, 3, 3),
                                child: fi,
                              ),
                            ),
                            GestureDetector(
                              onTap: (){
                                loadRandom();
                              },
                              child:Container(
                                color: appTheme.cardColor,
                                padding: const EdgeInsets.fromLTRB(3, 3, 3, 3),
                                child: const Icon(
                                  Icons.shuffle,
                                  color: Colors.white,
                                  size: 27,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
        break;
      default:
        return GestureDetector(
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => Detailcocktail.cocktail(cocktail: c)));
          },
          child: Container(
            height: MediaQuery.of(context).size.height/5,
            decoration: BoxDecoration(
              color: appTheme.cardColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 4,
                  child: SizedBox(
                    height: double.infinity,
                    child:ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                      ),
                      child:
                      Image.network(
                        '${c.strDrinkThumb ?? ""}/preview',
                        fit: BoxFit.cover,
                        errorBuilder:
                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return const Center(
                            child: CircularProgressIndicator(value: 1,),
                          );
                        },
                        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              c.strDrink ?? "",
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              c.getStrIngredient(),
                              overflow: TextOverflow.fade,
                              maxLines: 2,
                              softWrap: true,
                              style: const TextStyle(
                                color: Colors.white70,
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                await addOrRemoveDataToList('Cocktail',c.idDrink ?? "");
                                setState(() {
                                  fi=getFavoriteIcon(c.idDrink ?? "");
                                });
                              },
                              child:Container(
                                color: appTheme.cardColor,
                                padding: const EdgeInsets.fromLTRB(3, 3, 3, 3),
                                child: fi,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
    }

  }
}

