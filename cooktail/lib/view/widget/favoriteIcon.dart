import 'package:flutter/material.dart';
import 'package:cooktail/utils/cocktail_data.dart';

Future<Icon> loadFavorite(String id) async{
  if(await containsDataList('Cocktail', id)) {
    return const Icon(
      Icons.favorite,
      color: Colors.white,
      size: 27,
    );
  }
  else {
    return const Icon(
      Icons.favorite_outline,
      color: Colors.white70,
      size: 27,
    );
  }
}

FutureBuilder<Icon> getFavoriteIcon(String id){
  return FutureBuilder<Icon>(
    future: loadFavorite(id),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return const Icon(Icons.favorite_outline,color: Colors.white70,size: 27);
      } else if (snapshot.hasError) {
        return const Icon(Icons.favorite_outline,color: Colors.white70,size: 27);
      } else {
        return snapshot.data ?? const Icon(Icons.favorite_outline,color: Colors.white70,size: 27);
      }
    },
  );
}