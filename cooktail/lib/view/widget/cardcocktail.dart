import 'package:flutter/material.dart';
import 'package:cooktail/view/detailcocktail.dart';
import 'package:cooktail/models/models.dart';
import 'package:cocktail_repository/cocktail_repository.dart';
import 'package:cooktail/view/widget/favoriteIcon.dart';
import 'package:cooktail/utils/cocktail_data.dart';

enum ViewType{
  vanila,
  idDrink,
  favorite,
}

class Cardcocktail extends StatefulWidget {
  final String strDrinkThumb;
  final String strDrink;
  final String idDrink;
  final ViewType viewType;

  const Cardcocktail({required this.strDrink,required this.idDrink,required this.strDrinkThumb,super.key}) : viewType=ViewType.vanila;
  const Cardcocktail.idDrink({required this.idDrink,super.key}) : strDrinkThumb="----",strDrink="-----",viewType=ViewType.idDrink;
  const Cardcocktail.favorite({required this.idDrink,super.key}) : strDrinkThumb="----",strDrink="-----",viewType=ViewType.favorite;

  @override
  State<Cardcocktail> createState() => _CardcocktailState();
}

class _CardcocktailState extends State<Cardcocktail> {
  late String strDrinkThumb;
  late String strDrink;
  late String idDrink;
  late ViewType viewType;
  late Cocktail c=Cocktail.empty;
  FutureBuilder<Icon> fi=getFavoriteIcon("");
  
  void loadData() async{
    Cocktail_Repository cr = await CocktailRepository().getCocktailById(idDrink);
    setState(() {
      c = Cocktail.fromRepository(cr);
      fi=getFavoriteIcon(c.idDrink ?? "");
    });
  }

  @override
  void initState() {
    super.initState();
    strDrinkThumb = widget.strDrinkThumb;
    strDrink = widget.strDrink;
    idDrink = widget.idDrink;
    viewType = widget.viewType;
    if(viewType!=ViewType.vanila) {
      loadData();
    }
  }
  @override
  Widget build(BuildContext context) {
    switch(viewType){
    case ViewType.idDrink:
      return GestureDetector(
        onTap: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => Detailcocktail.cocktail(cocktail: c)));
        },
        child: Container(
          width: MediaQuery.of(context).size.width*3/8,
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width*3/8,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    child:Image.network(
                      '${c.strDrinkThumb ?? "http://test"}/preview',
                      fit: BoxFit.cover,
                      errorBuilder:
                          (BuildContext context, Object exception, StackTrace? stackTrace) {
                        return Container(color: Colors.black,);
                      },
                      loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                        if (loadingProgress == null) {
                          return child;
                        }
                        return Center(
                          child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes!
                                : null,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0, 2, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (c.strDrink ?? ""),
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      softWrap: false,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      (c.getStrIngredient()),
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      softWrap: false,
                      style: const TextStyle(
                        color: Colors.white70,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    case ViewType.favorite:
      return GestureDetector(
          onTap: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => Detailcocktail.cocktail(cocktail: c)));
        },
        child: Container(
          margin: const EdgeInsets.fromLTRB(0, 0, 0, 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    width: double.infinity,
                    margin: const EdgeInsets.fromLTRB(13, 0, 13, 0),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child:Image.network(
                      '${c.strDrinkThumb ?? ""}/preview',
                      fit: BoxFit.cover,
                        errorBuilder:
                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return Container(color: Colors.black,);
                        },
                      loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                        if (loadingProgress == null) {
                          return child;
                        }
                        return Center(
                          child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes!
                                : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(13, 0, 13, 0),
                  padding: const EdgeInsets.fromLTRB(6, 2, 2, 2),
                  decoration: const BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12),bottomRight: Radius.circular(12)),
                  ),
                  //margin: const EdgeInsets.fromLTRB(15, 2, 15, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              (c.strDrink ?? ""),
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              c.getStrIngredient(),
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: const TextStyle(
                                color: Colors.white70,
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () async{
                          await addOrRemoveDataToList('Cocktail',c.idDrink ?? "");
                          setState(() {
                            fi=getFavoriteIcon(c.idDrink ?? "");
                          });
                        },
                        child: Container(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          decoration: const BoxDecoration(
                            color: Colors.black26,
                            borderRadius: BorderRadius.all(Radius.circular(100)),
                          ),
                          child: fi,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
      );
      default:
        return GestureDetector(
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => Detailcocktail(idDrink: idDrink)));
          },
          child: Container(
            width: MediaQuery.of(context).size.width*3/8,
            margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width*3/8,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child:Image.network(
                        '$strDrinkThumb/preview',
                        fit: BoxFit.cover,
                        errorBuilder:
                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return Container(color: Colors.black,);
                        },
                        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 2, 0, 0),
                  child: Text(
                    strDrink,
                    overflow: TextOverflow.fade,
                    maxLines: 1,
                    softWrap: false,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
    }

  }
}

