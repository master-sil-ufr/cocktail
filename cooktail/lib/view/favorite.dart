import 'package:cooktail/theme.dart';
import 'package:cooktail/view/widget/cardcocktail.dart';
import 'package:flutter/material.dart';
import 'package:cooktail/utils/cocktail_data.dart';


class Favorite extends StatefulWidget {
  const Favorite({super.key});

  @override
  State<Favorite> createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  List<String> displayData=[];

  Container emptyFav() {
    return Container(
      margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.sms_failed_outlined,
            size: 80,
            color: Colors.blueGrey,
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              "Vous n'avez pas de cocktail en favoris",
              style: titleTextStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Text(
            "Ajouter de nouveau cocktail à vos favoris à l'aide du petit coeur !",
            style: bodyTextStyle,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          scrolledUnderElevation: 4.0,
          shadowColor: Theme.of(context).shadowColor,
          title: const Text(
            "Favoris",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 23,
            ),
          ),
          backgroundColor: Colors.black26,
        ),
        body: FutureBuilder<List<String>>(
            future: getDataList('Cocktail'),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                if (snapshot.data!=null && snapshot.data!.isEmpty) {
                  return emptyFav();
                }
                else {
                  displayData = snapshot.data ?? [];
                  return RefreshIndicator(
                    onRefresh: () async {
                      List<String> refreshedData = await getDataList('Cocktail');
                      setState(() {
                        displayData = refreshedData;
                      });
                    },
                    child: GridView.count(
                      crossAxisCount: 2,
                      children: List.generate(
                          displayData.length, (index) {
                        return Center(
                          child: Cardcocktail.favorite(idDrink: displayData.elementAt(index).toString()),
                        );
                      }),
                    ),
                  );
                }
              }
            },
          ),
      ),
    );
  }
}

