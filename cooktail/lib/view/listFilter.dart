import 'package:cooktail/models/cocktail.dart';
import 'package:flutter/material.dart';
import 'package:cooktail/utils/filtre.dart';
import 'package:cooktail/view/widget/cardcocktail.dart';
import 'package:cooktail/models/models.dart';
import 'package:cocktail_repository/cocktail_repository.dart';

class ListFilter extends StatefulWidget {
  final String initialFilter;
  final Filter filtre;
  const ListFilter({super.key,required this.initialFilter,required this.filtre});

  @override
  State<ListFilter> createState() => _ListFilterState();
}

class _ListFilterState extends State<ListFilter> {
  late String selectedFilter;
  late Filter filter;
  late List<String> dropdownMenuEntries;
  List<Cocktail> listCocktail=[];

  void loadDropdown() async{
    switch (filter){
      case Filter.c:
        List<Cocktail_Repository> cr = await CocktailRepository().getListOfCategories();
        for(Cocktail_Repository c in cr) {
          dropdownMenuEntries.add(Cocktail.fromRepository(c).strCategory ?? "NULL");
        }
        break;
      case Filter.g:
        List<Cocktail_Repository> cr = await CocktailRepository().getListOfGlasses();
        for(Cocktail_Repository c in cr) {
          dropdownMenuEntries.add(Cocktail.fromRepository(c).strGlass ?? "NULL");
        }
        break;
      case Filter.i:
        List<Cocktail_Repository> cr = await CocktailRepository().getListOfIngredients();
        for(Cocktail_Repository c in cr) {
          dropdownMenuEntries.add(Cocktail.fromRepository(c).strIngredient1 ?? "NULL");
        }
        break;
      case Filter.a:
        List<Cocktail_Repository> cr = await CocktailRepository().getListOfAlcoholics();
        for(Cocktail_Repository c in cr) {
          dropdownMenuEntries.add(Cocktail.fromRepository(c).strAlcoholic ?? "NULL");
        }
        break;
      default:
        break;
    }
    setState(() {
      dropdownMenuEntries.sort();
    });
  }

  void loadCocktail() async{
    setState(() {listCocktail.clear();});
    List<Cocktail_Repository> cr=[];
    switch (filter){
      case Filter.c:
        cr=await CocktailRepository().filterByCategorie(selectedFilter);
        break;
      case Filter.g:
        cr=await CocktailRepository().filterByGlass(selectedFilter);
        break;
      case Filter.i:
        cr=await CocktailRepository().filterByIngredient(selectedFilter);
        break;
      case Filter.a:
        cr=await CocktailRepository().filterByAlcoholic(selectedFilter);
        break;
      default:
        break;
    }
    setState(() {
      for(Cocktail_Repository c in cr) {
        listCocktail.add(Cocktail.fromRepository(c));
      }
    });
  }

  @override
  void initState() {
    super.initState();
    selectedFilter = widget.initialFilter;
    filter = widget.filtre;
    dropdownMenuEntries=[];
    loadDropdown();
    loadCocktail();
  }

  Text titre() {
    String titre="";
    switch (filter){
      case Filter.c:
        titre="Catégorie";
        break;
      case Filter.g:
        titre="Verre";
        break;
      case Filter.i:
        titre="Ingrédient";
        break;
      case Filter.a:
        titre="Alcool";
        break;
      default:
    }
    return Text(
      titre,
      style: const TextStyle(
        color: Colors.white70,
        fontSize: 22,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: titre(),
        backgroundColor: Colors.black26,
      ),
      body:Column(
        children:[
          Container(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            alignment: Alignment.center,
            child: DropdownButton(
              onChanged: (texte) {
                selectedFilter=texte.toString();
                loadCocktail();
              },
              dropdownColor: const Color.fromRGBO(47, 55, 56, 1.0),
              value: selectedFilter,
              items: dropdownMenuEntries
                  .map<DropdownMenuItem<String>>(
                      (String texte) {
                    return DropdownMenuItem<String>(
                      value: texte,
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width*5/6,
                        child: Text(
                          texte,
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          softWrap: true,
                          style: const TextStyle(fontSize: 22,color: Colors.white),
                        ),
                      ),
                    );
                  }).toList(),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: GridView.count(
                crossAxisCount: 2,
                children: List.generate(listCocktail.length, (index) {
                  return Container(
                    margin: const EdgeInsets.only(top:10),
                    child:Center(
                      child: Cardcocktail(strDrink: listCocktail.elementAt(index).strDrink ?? "",idDrink: listCocktail.elementAt(index).idDrink ?? "",strDrinkThumb: listCocktail.elementAt(index).strDrinkThumb ?? ""),
                    ),
                  );
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

