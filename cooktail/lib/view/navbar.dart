import 'package:cooktail/theme.dart';
import 'package:flutter/material.dart';
import 'package:cooktail/view/home.dart';
import 'package:cooktail/view/search.dart';
import 'package:cooktail/view/favorite.dart';



class Navigation extends StatefulWidget {
  const Navigation({super.key});

  @override
  State<Navigation> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  int currentPageIndex = 0;

  Container item(int index) {
    IconData i;
    String s;
    if (currentPageIndex==index) {
        switch(index){
          case 0:
            i=Icons.home;
            s="Accueil";
            break;
          case 1:
            i=Icons.search;
            s="Recherche";
            break;
          default:
            i=Icons.favorite;
            s="Favoris";
            break;
        }
        return Container(
          height: 50,
          width: MediaQuery.of(context).size.width/4,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            children: <Widget>[
              Icon(i, color: Colors.white,),
              Text(s, style: const TextStyle(
                color: Colors.white,
              ),),
              Container(
                height: 4,
                width: 25,
                margin: const EdgeInsets.fromLTRB(0, 3, 0, 0),
                decoration: const BoxDecoration(
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                ),
              ),
            ],
          ),
        );
    }
    else{
      switch(index){
        case 0:
          i=Icons.home_outlined;
          s="Accueil";
          break;
        case 1:
          i=Icons.search_outlined;
          s="Recherche";
          break;
        default:
          i=Icons.favorite_outline;
          s="Favoris";
          break;
      }
      return Container(
        height: 50,
        width: MediaQuery.of(context).size.width/4,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: <Widget>[
            Icon(i, color: Colors.white70,),
            Text(s, style: const TextStyle(
              color: Colors.white70,
            ),),
          ],
        ),
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(10),
          margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width/12,vertical: 15),
          decoration: BoxDecoration(
            color: appTheme.cardColor,
            borderRadius: const BorderRadius.all(Radius.circular(24)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.4),
                spreadRadius: 1,
                blurRadius: 5,
                offset: const Offset(1, 1), // changes position of shadow
              ),
            ],
          ),

          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ...List.generate(3, (index) => GestureDetector(
                  onTap: (){
                    setState(() {
                      currentPageIndex = index;
                    });
                  },
                  child: item(index)
                  ),
              ),
            ],
          ),
        ),
      ),
      body: <Widget>[
        const Home(),
        const Search(),
        const Favorite(),
      ][currentPageIndex],
    );
  }
}



