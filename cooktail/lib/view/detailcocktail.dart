import 'package:flutter/material.dart';
import 'package:cooktail/theme.dart';
import 'package:share/share.dart';
import 'package:cooktail/models/models.dart';
import 'package:cooktail/utils/cocktail_data.dart';
import 'package:cooktail/view/widget/favoriteIcon.dart';
import 'package:cocktail_repository/cocktail_repository.dart';

class Detailcocktail extends StatefulWidget {
  final Cocktail cocktail;
  final String idDrink;

  Detailcocktail({super.key, required this.idDrink})  : cocktail=Cocktail.empty;
  Detailcocktail.cocktail({super.key, required this.cocktail}) : idDrink=(cocktail.idDrink ?? "");

  @override
  State<Detailcocktail> createState() => _DetailcocktailState();
}

class _DetailcocktailState extends State<Detailcocktail> {
  late String? idDrink;
  late Cocktail cocktail;
  FutureBuilder<Icon> fi=getFavoriteIcon("");
  @override
  void initState() {
    super.initState();
    idDrink = widget.idDrink;
    cocktail = widget.cocktail;
    removeAndAddDataToList('LastConsult',idDrink ?? "");
    if(cocktail==Cocktail.empty) {
      loadData();
    }
    setState(() {
      fi=getFavoriteIcon(idDrink ?? "");
    });
  }

  void loadData() async{
    Cocktail_Repository cr = await CocktailRepository().getCocktailById(idDrink ?? "");
    setState(() {
      cocktail = Cocktail.fromRepository(cr);
    });
  }

  /*String description(){
    switch(country.countryCode){
      case 'es':
        return cocktail.strInstructionsES ?? cocktail.strInstructions ?? "";
      case 'de':
        return cocktail.strInstructionsDE ?? cocktail.strInstructions ?? "";
      case 'fr':
        return cocktail.strInstructionsFR ?? cocktail.strInstructions ?? "";
      case 'it':
        return cocktail.strInstructionsIT ?? cocktail.strInstructions ?? "";
      default:
        return cocktail.strInstructions ?? "";
    }
  }*/

  Widget degres(Ingredient i){
    if(i.strABV!=null) {
      return Text(
        "${i.strABV ?? ""}°C",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      );
    }
    else {
      return Container();
    }
  }

  Widget type(Ingredient i){
    if(i.strType!=null) {
      return Row(
        children: [
          Image.network(
            "https://cdn.icon-icons.com/icons2/2406/PNG/512/tags_categories_icon_145927.png",
            height: 25,
            color: Colors.white,
          ),
          Text(
            " ${i.strType ?? ""}",
            overflow: TextOverflow.fade,
            maxLines: 1,
            softWrap: false,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ],
      );
    }
    else {
      return Container();
    }
  }

  void _showModalBottomSheet(BuildContext context,String ingredient) async{
    Ingredient_Repository ir = await CocktailRepository().searchIngredientByName(ingredient);
    Ingredient i = Ingredient.fromRepository(ir);
    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: const Color.fromRGBO(1, 1, 1, 0),
      context: context,
      builder: (BuildContext context) {
        if(i.strDescription!=null){
          return SizedBox.expand(
            child: DraggableScrollableSheet(
              maxChildSize: 0.75,
              minChildSize: 0.2,
              initialChildSize: 0.25,
              builder: (BuildContext context, ScrollController scrollController) {
                return  Container(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(58, 67, 75, 1.0),
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(25),topRight: Radius.circular(25)),
                  ),
                  child: ListView.builder(
                  controller: scrollController,
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          const Icon(Icons.menu,color: Colors.white,size: 19,),
                          Container(
                            margin: const EdgeInsets.fromLTRB(10,10,10,10),
                            child: Row(
                              children: [
                                Expanded(
                                  flex:2,
                                  child: Text(
                                    ingredient,
                                    overflow: TextOverflow.fade,
                                    maxLines: 2,
                                    softWrap: true,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 25,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                                Column(
                                  children: [
                                    type(i),
                                    degres(i),
                                  ],
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(15),

                            decoration: BoxDecoration(
                              color: appTheme.cardColor,
                              borderRadius: const BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.4),
                                  spreadRadius: 1,
                                  blurRadius: 5,
                                  offset: const Offset(1, 1),
                                ),
                              ],
                            ),
                            child: Text(
                              i.strDescription ?? "",
                              textAlign: TextAlign.justify,
                              style: const TextStyle(
                                color: Colors.white54,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                );
              },
            ),
          );
        }
        else{
          return SizedBox.expand(
            child: DraggableScrollableSheet(
              maxChildSize: 0.13,
              minChildSize: 0.10,
              initialChildSize: 0.13,
              builder: (BuildContext context, ScrollController scrollController) {
                return  Container(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(58, 67, 75, 1.0),
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(25),topRight: Radius.circular(25)),
                  ),
                  child: ListView.builder(
                    controller: scrollController,
                    itemCount: 1,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          const Icon(Icons.menu,color: Colors.white,size: 19,),
                          Container(
                            margin: const EdgeInsets.fromLTRB(10,10,10,10),
                            child: Row(
                              children: [
                                Expanded(
                                  flex:2,
                                  child: Text(
                                    ingredient,
                                    overflow: TextOverflow.fade,
                                    maxLines: 2,
                                    softWrap: true,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 25,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                                Column(
                                  children: [
                                    type(i),
                                    degres(i),
                                  ],
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                );
              },
            ),
          );
        }
      },
    );
  }
  List<InkWell> ingredient(){
    List<InkWell> c=[];
    for(int i=1;i<=cocktail.ingredientLength();i++){
      c.add(
          InkWell(
            onTap: () {
              _showModalBottomSheet(context,cocktail.getStringIngredient(i));
            },
            child: Container(
            margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
            decoration: BoxDecoration(
              color: appTheme.cardColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.4),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: const Offset(1, 1), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children:[
                Text(
                  cocktail.getStringIngredient(i),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  maxLines: 2,
                  softWrap: true,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                  ),
                ),
                Text(
                  cocktail.getStringMeasure(i),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  maxLines: 1,
                  softWrap: false,
                  style: const TextStyle(
                    color: Colors.white70,
                    fontSize: 21,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return c;
  }

  Widget detail(String url,String text){
    if(text!="") {
      return Expanded(child:Container(
        width: MediaQuery.of(context).size.width*3/11,
        margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        padding: const EdgeInsets.fromLTRB(7, 13, 7, 13),
        decoration: BoxDecoration(
          color: appTheme.cardColor,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.4),
              spreadRadius: 1,
              blurRadius: 5,
              offset: const Offset(1, 1), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          children: [
            Image.network(
              url,
              color: Colors.white,
              height: 37,
              errorBuilder:
                  (BuildContext context, Object exception,
                  StackTrace? stackTrace) {
                return Container(color: Colors.black,);
              },
              loadingBuilder: (BuildContext context, Widget child,
                  ImageChunkEvent? loadingProgress) {
                if (loadingProgress == null) {
                  return child;
                }
                return Container(color: Colors.black,);
              },
            ),
            Container(
              margin: const EdgeInsets.only(top: 7),
              child: Text(
                text,
                overflow: TextOverflow.fade,
                maxLines: 1,
                softWrap: false,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ),
      ));
    }
    else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            forceElevated: true,
            elevation: 10,
            backgroundColor: appTheme.backgroundColor,
            shadowColor: Colors.black,
            iconTheme: const IconThemeData(color: Colors.white,size: 27),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () async{
                    await Share.share('J\'ai trouvé un brevage qui pourrait te plaire !!! \n \n https://www.thecocktaildb.com/drink/${cocktail.idDrink ?? ""}');//+idDrink
                  },
                  child: Container(
                    color: appTheme.backgroundColor,
                    padding: const EdgeInsets.fromLTRB(7, 5, 7, 5),
                    child: const Icon(Icons.share_outlined,color: Colors.white,size: 27),
                  ),
                ),
                GestureDetector(
                  onTap: () async{
                    await addOrRemoveDataToList('Cocktail',cocktail.idDrink ?? "");
                    setState(() {
                      fi=getFavoriteIcon(cocktail.idDrink ?? "");
                    });
                  },
                  child: Container(
                    color: appTheme.backgroundColor,
                    padding: const EdgeInsets.fromLTRB(7, 5, 7, 5),
                    child: fi,
                  ),
                ),
              ],
            ),
            floating: false,
            pinned: true,
            snap: false,
          ),
          SliverToBoxAdapter(
            child: SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(24)),
                    ),
                    child:ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      child:
                      Image.network(
                        cocktail.strDrinkThumb ?? "",
                        errorBuilder:
                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                              return Container(color: Colors.black,);
                        },
                        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                    child: Text(
                      cocktail.strDrink ?? "",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      detail("https://cdn.icon-icons.com/icons2/1915/PNG/512/emptyglass_121617.png",cocktail.strGlass ?? ""),
                      detail("https://cdn.icon-icons.com/icons2/2406/PNG/512/tags_categories_icon_145927.png",cocktail.strCategory ?? ""),
                      detail("https://cdn-icons-png.flaticon.com/512/13/13938.png",cocktail.strAlcoholic ?? ""),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    child:const Text(
                      "Instruction",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 23,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                    padding: const EdgeInsets.all(15),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: appTheme.cardColor,
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.4),
                          spreadRadius: 1,
                          blurRadius: 5,
                          offset: const Offset(1, 1),
                        ),
                      ],
                    ),
                    child: Text(
                      (cocktail.strInstructions ?? "") != "" ? cocktail.strInstructions! : "Aucune instruction",
                      textAlign: TextAlign.justify,
                      style: const TextStyle(
                        color: Colors.white54,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    child:const Text(
                      "Ingrédients",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 23,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child:
                    Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        ...ingredient(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

