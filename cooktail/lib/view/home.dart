import 'dart:math';

import 'package:cooktail/theme.dart';
import 'package:cooktail/view/widget/cardcocktail.dart';
import 'package:flutter/material.dart';
import 'package:cooktail/view/widget/fullWeightCocktail.dart';
import 'package:cooktail/view/listFilter.dart';
import 'package:cooktail/utils/filtre.dart';
import 'package:cooktail/utils/cocktail_data.dart';
import 'package:cocktail_repository/cocktail_repository.dart';
import 'package:cooktail/models/models.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Cocktail> listGlass=[];
  List<Cocktail> listAlcoholic=[];
  List<Cocktail> listIngredient=[];
  List<Cocktail> listCategory=[];
  String strGlass="";
  String strAlcoholic="";
  String strIngredient="";
  String strCategory="";

  Container separeLine() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 16, 10, 13),
      height: 1,
      decoration: const BoxDecoration(
        color: Colors.white24,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }

  Container title(String texte) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: Text(
        texte,
        style: const TextStyle(
          color: Colors.white70,
          fontSize: 22,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  FutureBuilder<List<String>> historique() {
    return FutureBuilder<List<String>>(
      future: getDataList('LastConsult'),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          if (snapshot.data!=null && snapshot.data!.isEmpty) {
            return Container();
          }
          else {
            return SizedBox(
              height: MediaQuery.of(context).size.height*3/10,
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                title("Dernière consultation"),
                  Expanded(
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: snapshot.data!.length,
                      itemBuilder: (context, index) {
                        return Cardcocktail.idDrink(idDrink: snapshot.data!.elementAt(index).toString());
                      },
                    ),
                  ),
                  separeLine(),
                ],
              ),
            );
          }
        }
      },
    );
  }

  Column listFilter(String valeur,String type,List<Cocktail> list, Filter f) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListFilter(initialFilter: valeur,filtre: f,)));
          },
          child: Container(
            color: appTheme.backgroundColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: title(valeur),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:[
                    Text(type,style: TextStyle(color: Colors.white70),),
                    const Icon(Icons.chevron_right, color: Colors.white70, size: 30,)
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height/5,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: list.length,
            itemBuilder: (context, index) {
              return Cardcocktail(strDrink: list.elementAt(index).strDrink ?? "",idDrink: list.elementAt(index).idDrink ?? "",strDrinkThumb: list.elementAt(index).strDrinkThumb ?? "");
            },
          ),
        ),
      ],
    );
  }

  void loadData() async{
    List<Cocktail_Repository> c1 = await CocktailRepository().getListOfAlcoholics();
    strAlcoholic=Cocktail.fromRepository(c1.elementAt(Random().nextInt(c1.length))).strAlcoholic ?? "";
    c1=await CocktailRepository().filterByAlcoholic(strAlcoholic);
    for(Cocktail_Repository c in c1) {
      listAlcoholic.add(Cocktail.fromRepository(c));
    }
    c1 = await CocktailRepository().getListOfIngredients();
    strIngredient=Cocktail.fromRepository(c1.elementAt(Random().nextInt(c1.length))).strIngredient1 ?? "";
    c1=await CocktailRepository().filterByIngredient(strIngredient);
    for(Cocktail_Repository c in c1) {
      listIngredient.add(Cocktail.fromRepository(c));
    }
    c1 = await CocktailRepository().getListOfGlasses();
    strGlass=Cocktail.fromRepository(c1.elementAt(Random().nextInt(c1.length))).strGlass ?? "";
    c1=await CocktailRepository().filterByGlass(strGlass);
    for(Cocktail_Repository c in c1) {
      listGlass.add(Cocktail.fromRepository(c));
    }
    c1 = await CocktailRepository().getListOfCategories();
    strCategory=Cocktail.fromRepository(c1.elementAt(Random().nextInt(c1.length))).strCategory ?? "";
    c1=await CocktailRepository().filterByCategorie(strCategory);
    for(Cocktail_Repository c in c1) {
      listCategory.add(Cocktail.fromRepository(c));
    }

    setState(() {
      listAlcoholic.shuffle();
      listGlass.shuffle();
      listCategory.shuffle();
      listIngredient.shuffle();
    });
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          const SliverAppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Cook',
                  style: TextStyle(
                    color: Color.fromRGBO(120, 144, 156, 1.0),
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Hind',
                  ),
                ),
                Text(
                  'Tail',
                  style: TextStyle(
                    color: Colors.blueGrey,
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Hind',
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ],
            ),
            centerTitle: true,
            floating: false,
            pinned: false,
            snap: false,
            expandedHeight: 70.0,
            backgroundColor: Color.fromRGBO(0, 0, 0, 0),
          ),
          SliverToBoxAdapter(
            child: SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  title("Suggestion aléatoire"),
                  const FullWeightCocktail(),separeLine(),
                  historique(),
                  listFilter(strCategory,"Catégorie",listCategory,Filter.c),separeLine(),
                  listFilter(strIngredient,"Ingrédient",listIngredient,Filter.i),separeLine(),
                  listFilter(strAlcoholic,"Alcool",listAlcoholic,Filter.a),separeLine(),
                  listFilter(strGlass,"Verre",listGlass,Filter.g),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
