import 'package:cooktail/theme.dart';
import 'package:cooktail/view/widget/fullWeightCocktail.dart';
import 'package:flutter/material.dart';
import 'package:cooktail/utils/cocktail_data.dart';
import 'package:cooktail/models/models.dart';
import 'package:cocktail_repository/cocktail_repository.dart';

enum Sort{
  alphbetique,
  nalphbetique,
  nvIngCroissant,
  nvIngDecroissant,
}

enum Critere{
  name,
  ingredient,
  firstLetter,
}

class Search extends StatefulWidget {
  const Search({super.key});

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  List<Cocktail> listCocktail=[];
  Sort sort=Sort.alphbetique;
  Critere critere=Critere.name;
  String recherche="";

  void tri() {
    switch(sort){
      case Sort.nvIngCroissant:
        setState(() {
          listCocktail.sort((a, b) => a.ingredientLength().compareTo(b.ingredientLength()));
        });
        break;
      case Sort.nvIngDecroissant:
        setState(() {
          listCocktail.sort((a, b) => b.ingredientLength().compareTo(a.ingredientLength()));
        });
        break;
      case Sort.nalphbetique:
        setState(() {
          listCocktail.sort((a, b) => (b.strDrink ?? "").compareTo((a.strDrink ?? "")));
        });
        break;
      default:
        setState(() {
          listCocktail.sort((a, b) => (a.strDrink ?? "").compareTo((b.strDrink ?? "")));
        });
        break;
    }
  }

  Future<void> search() async{
    setState(() {
      listCocktail.clear();
    });
    List<Cocktail_Repository> c1=[];

    switch(critere){
      case Critere.ingredient:
        Ingredient_Repository i1=await CocktailRepository().searchIngredientByName(recherche);
        c1=await CocktailRepository().filterByIngredient(i1.strIngredient ?? "");
        break;
      case Critere.firstLetter:
        c1=await CocktailRepository().listCocktailByFirstLetter(recherche[0]);
        break;
      default:
        c1=await CocktailRepository().searchCocktailByName(recherche);
        break;
    }
    for(Cocktail_Repository c in c1){
      listCocktail.add(Cocktail.fromRepository(c));
    }
    tri();
  }



  void _showRadioButtonDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                ListTile(
                  title: const Text('Tri alphabétique'),
                  onTap: (){
                    bool haveChange=sort!=Sort.alphbetique;
                    sort = Sort.alphbetique;
                    if(haveChange) {
                      search();
                    }
                    Navigator.pop(context);
                  },
                  leading: Radio(
                    value: Sort.alphbetique,
                    groupValue: sort,
                    onChanged: (value) {
                      bool haveChange=sort!=Sort.alphbetique;
                      sort = Sort.alphbetique;
                      if(haveChange) {
                        search();
                      }
                      Navigator.pop(context);
                    },
                  ),
                ),
                ListTile(
                  onTap: (){
                    bool haveChange=sort!=Sort.nalphbetique;
                    sort = Sort.nalphbetique;
                    if(haveChange) {
                      search();
                    }
                    Navigator.pop(context);
                  },
                  title: const Text('Tri alphabétique inversé'),
                  leading: Radio(
                    value: Sort.nalphbetique,
                    groupValue: sort,
                    onChanged: (value) {
                      bool haveChange=sort!=Sort.nalphbetique;
                      sort = Sort.nalphbetique;
                      if(haveChange) {
                        search();
                      }
                      Navigator.pop(context);
                    },
                  ),
                ),
                ListTile(
                  onTap: (){
                    bool haveChange=sort!=Sort.nvIngCroissant;
                    sort = Sort.nvIngCroissant;
                    if(haveChange) {
                      search();
                    }
                    Navigator.pop(context);
                  },
                  title: const Wrap(children: [Text('Nombre d\'ingrédient'),Icon(Icons.call_made)],),
                  leading: Radio(
                    value: Sort.nvIngCroissant,
                    groupValue: sort,
                    onChanged: (value) {
                      bool haveChange=sort!=Sort.nvIngCroissant;
                      sort = Sort.nvIngCroissant;
                      if(haveChange) {
                        search();
                      }
                      Navigator.pop(context);
                    },
                  ),
                ),
                ListTile(
                  onTap: (){
                    bool haveChange=sort!=Sort.nvIngDecroissant;
                    sort = Sort.nvIngDecroissant;
                    if(haveChange) {
                      search();
                    }
                    Navigator.pop(context);
                  },
                  title: const Wrap(children: [Text('Nombre d\'ingrédient'),Icon(Icons.call_received)],),
                  leading: Radio(
                    value: Sort.nvIngDecroissant,
                    groupValue: sort,
                    onChanged: (value) {
                      bool haveChange=sort!=Sort.nvIngDecroissant;
                      sort = Sort.nvIngDecroissant;
                      if(haveChange) {
                        search();
                      }
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if(listCocktail.isNotEmpty) {
      return Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(20, 40, 20, 0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SearchAnchor(
                builder: (BuildContext context, SearchController controller) {
                  return SearchBar(
                    controller: controller,
                    padding: const MaterialStatePropertyAll<EdgeInsets>(
                        EdgeInsets.only(left: 16.0, right: 10)
                    ),
                    onSubmitted: (String query) async {
                      recherche=query;
                      await search();
                    },
                    hintText: "Que veux-tu boire ?",
                    hintStyle: MaterialStateTextStyle.resolveWith(
                          (states) =>
                      const TextStyle(
                        color: Colors.black54,
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    trailing: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(6, 6, 6, 6),
                        decoration: const BoxDecoration(
                          color: Colors.blueGrey,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                        ),
                        child: const Icon(
                          Icons.search, color: Colors.white, size: 30,),
                      )
                    ],
                  );
                },
                suggestionsBuilder: (BuildContext context,
                    SearchController controller) async {
                  List<String> recentSearches = await getDataList(
                      "recentSearch");
                  return recentSearches.map((String item) {
                    return ListTile(
                      title: Text(item),
                      onTap: () {
                        setState(() {
                          controller.closeView(item);
                        });
                      },
                    );
                  }).toList();
                },
              ),
              SizedBox(
                height: 50,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _showRadioButtonDialog();
                      },
                      child: Container(
                        margin: const EdgeInsets.only(right: 10),
                        padding: const EdgeInsets.all(7),
                        decoration: BoxDecoration(
                          color: const Color.fromRGBO(1, 1, 1, 0),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: const Icon(
                          Icons.sort,
                          color:Colors.white,
                          size: 30,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: ChoiceChip(
                        backgroundColor: Colors.black54,
                        label: const Text(
                          'Nom',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontSize: 17
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                        selected: critere == Critere.name,
                        onSelected: (bool selected) {
                          setState(() {
                            critere = Critere.name;
                          });
                          search();
                        },
                        selectedColor: Colors.blueGrey,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: ChoiceChip(
                        backgroundColor: Colors.black54,
                        label: const Text(
                          'Ingrédient',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontSize: 17
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                        selected: critere == Critere.ingredient,
                        onSelected: (bool selected) {
                          setState(() {
                            critere = Critere.ingredient;
                          });
                          search();
                        },
                        selectedColor: Colors.blueGrey,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: ChoiceChip(
                        backgroundColor: Colors.black54,
                        label: const Text(
                          'Première lettre',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontSize: 17
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                        selected: critere == Critere.firstLetter,
                        onSelected: (bool selected) {
                          setState(() {
                            critere = Critere.firstLetter;
                          });
                          search();
                        },
                        selectedColor: Colors.blueGrey,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                  padding: const EdgeInsets.all(0),
                  scrollDirection: Axis.vertical,
                  itemCount: listCocktail.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      child: FullWeightCocktail(cocktail: listCocktail.elementAt(index)),
                    );
                  }
                ),
              ),
            ],
          ),
        ),
      );
    }
    else{
      return Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(20, 40, 20, 20),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SearchAnchor(
                builder: (BuildContext context, SearchController controller) {
                  return SearchBar(
                    controller: controller,
                    padding: const MaterialStatePropertyAll<EdgeInsets>(
                        EdgeInsets.only(left: 16.0, right: 10)
                    ),
                    onSubmitted: (String query) async {
                      recherche=query;
                      await search();
                    },
                    hintText: "Que veux-tu boire ?",
                    hintStyle: MaterialStateTextStyle.resolveWith(
                          (states) =>
                      const TextStyle(
                        color: Colors.black54,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    trailing: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(6, 6, 6, 6),
                        decoration: const BoxDecoration(
                          color: Colors.blueGrey,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                        ),
                        child: const Icon(
                          Icons.search, color: Colors.white, size: 30,),
                      )
                    ],
                  );
                },
                suggestionsBuilder: (BuildContext context,
                    SearchController controller) async {
                  List<String> recentSearches = await getDataList(
                      "recentSearch");
                  return recentSearches.map((String item) {
                    return ListTile(
                      title: Text(item),
                      onTap: () {
                        setState(() {
                          controller.closeView(item);
                        });
                      },
                    );
                  }).toList();
                },
              ),
              SizedBox(
                height: 50,
                  child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _showRadioButtonDialog();
                      },
                      child: Container(
                        margin: const EdgeInsets.only(right: 10),
                        padding: const EdgeInsets.all(7),
                        decoration: BoxDecoration(
                          color: appTheme.hoverColor,
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: const Icon(
                          Icons.sort,
                          color:Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: ChoiceChip(
                        backgroundColor: Colors.black54,
                        label: const Text(
                          'Nom',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontSize: 17
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                        selected: critere == Critere.name,
                        onSelected: (bool selected) {
                          setState(() {
                            critere = Critere.name;
                          });
                        },
                        selectedColor: Colors.blueGrey,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: ChoiceChip(
                        backgroundColor: Colors.black54,
                        label: const Text(
                          'Ingrédient',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontSize: 17
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                        selected: critere == Critere.ingredient,
                        onSelected: (bool selected) {
                          setState(() {
                            critere = Critere.ingredient;
                          });
                        },
                        selectedColor: Colors.blueGrey,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: ChoiceChip(
                        backgroundColor: Colors.black54,
                        label: const Text(
                          'Première lettre',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontSize: 17
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                        selected: critere == Critere.firstLetter,
                        onSelected: (bool selected) {
                          setState(() {
                            critere = Critere.firstLetter;
                          });
                        },
                        selectedColor: Colors.blueGrey,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}

