import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cocktail_repository/cocktail_repository.dart' as cocktail_repository;

part 'cocktail.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Cocktail extends Equatable {
  const Cocktail({
    required this.idDrink,
    required this.strDrink,
    required this.strDrinkAlternate,
    required this.strTags,
    required this.strVideo,
    required this.strCategory,
    required this.strIBA,
    required this.strAlcoholic,
    required this.strGlass,
    required this.strInstructions,
    required this.strInstructionsES,
    required this.strInstructionsDE,
    required this.strInstructionsFR,
    required this.strInstructionsIT,
    required this.strInstructionsZH_HANS,
    required this.strInstructionsZH_HANT,
    required this.strDrinkThumb,
    required this.strIngredient1,
    required this.strIngredient2,
    required this.strIngredient3,
    required this.strIngredient4,
    required this.strIngredient5,
    required this.strIngredient6,
    required this.strIngredient7,
    required this.strIngredient8,
    required this.strIngredient9,
    required this.strIngredient10,
    required this.strIngredient11,
    required this.strIngredient12,
    required this.strIngredient13,
    required this.strIngredient14,
    required this.strIngredient15,
    required this.strMeasure1,
    required this.strMeasure2,
    required this.strMeasure3,
    required this.strMeasure4,
    required this.strMeasure5,
    required this.strMeasure6,
    required this.strMeasure7,
    required this.strMeasure8,
    required this.strMeasure9,
    required this.strMeasure10,
    required this.strMeasure11,
    required this.strMeasure12,
    required this.strMeasure13,
    required this.strMeasure14,
    required this.strMeasure15,
    required this.strImageSource,
    required this.strImageAttribution,
    required this.strCreativeCommonsConfirmed,
    required this.dateModified,
  });

  factory Cocktail.fromJson(Map<String, dynamic> json) =>
      _$CocktailFromJson(json);

  factory Cocktail.fromRepository(cocktail_repository.Cocktail_Repository CocktailRepository) {
    return Cocktail(
      idDrink : CocktailRepository.idDrink,
      strDrink : CocktailRepository.strDrink,
      strDrinkAlternate : CocktailRepository.strDrinkAlternate,
      strTags : CocktailRepository.strTags,
      strVideo : CocktailRepository.strVideo,
      strCategory : CocktailRepository.strCategory,
      strIBA : CocktailRepository.strIBA,
      strAlcoholic : CocktailRepository.strAlcoholic,
      strGlass : CocktailRepository.strGlass,
      strInstructions : CocktailRepository.strInstructions,
      strInstructionsES : CocktailRepository.strInstructionsES,
      strInstructionsDE : CocktailRepository.strInstructionsDE,
      strInstructionsFR : CocktailRepository.strInstructionsFR,
      strInstructionsIT : CocktailRepository.strInstructionsIT,
      strInstructionsZH_HANS : CocktailRepository.strInstructionsZH_HANS,
      strInstructionsZH_HANT : CocktailRepository.strInstructionsZH_HANT,
      strDrinkThumb : CocktailRepository.strDrinkThumb,
      strIngredient1 : CocktailRepository.strIngredient1,
      strIngredient2 : CocktailRepository.strIngredient2,
      strIngredient3 : CocktailRepository.strIngredient3,
      strIngredient4 : CocktailRepository.strIngredient4,
      strIngredient5 : CocktailRepository.strIngredient5,
      strIngredient6 : CocktailRepository.strIngredient6,
      strIngredient7 : CocktailRepository.strIngredient7,
      strIngredient8 : CocktailRepository.strIngredient8,
      strIngredient9 : CocktailRepository.strIngredient9,
      strIngredient10 : CocktailRepository.strIngredient10,
      strIngredient11 : CocktailRepository.strIngredient11,
      strIngredient12 : CocktailRepository.strIngredient12,
      strIngredient13 : CocktailRepository.strIngredient13,
      strIngredient14 : CocktailRepository.strIngredient14,
      strIngredient15 : CocktailRepository.strIngredient15,
      strMeasure1 : CocktailRepository.strMeasure1,
      strMeasure2 : CocktailRepository.strMeasure2,
      strMeasure3 : CocktailRepository.strMeasure3,
      strMeasure4 : CocktailRepository.strMeasure4,
      strMeasure5 : CocktailRepository.strMeasure5,
      strMeasure6 : CocktailRepository.strMeasure6,
      strMeasure7 : CocktailRepository.strMeasure7,
      strMeasure8 : CocktailRepository.strMeasure8,
      strMeasure9 : CocktailRepository.strMeasure9,
      strMeasure10 : CocktailRepository.strMeasure10,
      strMeasure11 : CocktailRepository.strMeasure11,
      strMeasure12 : CocktailRepository.strMeasure12,
      strMeasure13 : CocktailRepository.strMeasure13,
      strMeasure14 : CocktailRepository.strMeasure14,
      strMeasure15 : CocktailRepository.strMeasure15,
      strImageSource : CocktailRepository.strImageSource,
      strImageAttribution : CocktailRepository.strImageAttribution,
      strCreativeCommonsConfirmed : CocktailRepository.strCreativeCommonsConfirmed,
      dateModified : CocktailRepository.dateModified,
    );
  }

  static final empty = Cocktail(
    idDrink: "",
    strDrink: "",
    strDrinkAlternate: "",
    strTags: "",
    strVideo: "",
    strCategory: "",
    strIBA: "",
    strAlcoholic: "",
    strGlass: "",
    strInstructions: "",
    strInstructionsES: "",
    strInstructionsDE: "",
    strInstructionsFR: "",
    strInstructionsIT: "",
    strInstructionsZH_HANS: "",
    strInstructionsZH_HANT: "",
    strDrinkThumb: "",
    strIngredient1: "",
    strIngredient2: "",
    strIngredient3: "",
    strIngredient4: "",
    strIngredient5: "",
    strIngredient6: "",
    strIngredient7: "",
    strIngredient8: "",
    strIngredient9: "",
    strIngredient10: "",
    strIngredient11: "",
    strIngredient12: "",
    strIngredient13: "",
    strIngredient14: "",
    strIngredient15: "",
    strMeasure1: "",
    strMeasure2: "",
    strMeasure3: "",
    strMeasure4: "",
    strMeasure5: "",
    strMeasure6: "",
    strMeasure7: "",
    strMeasure8: "",
    strMeasure9: "",
    strMeasure10: "",
    strMeasure11: "",
    strMeasure12: "",
    strMeasure13: "",
    strMeasure14: "",
    strMeasure15: "",
    strImageSource: "",
    strImageAttribution: "",
    strCreativeCommonsConfirmed: "",
    dateModified: "",
  );

  final String? idDrink;
  final String? strDrink;
  final String? strDrinkAlternate;
  final String? strTags;
  final String? strVideo;
  final String? strCategory;
  final String? strIBA;
  final String? strAlcoholic;
  final String? strGlass;
  final String? strInstructions;
  final String? strInstructionsES;
  final String? strInstructionsDE;
  final String? strInstructionsFR;
  final String? strInstructionsIT;
  final String? strInstructionsZH_HANS;
  final String? strInstructionsZH_HANT;
  final String? strDrinkThumb;
  final String? strIngredient1;
  final String? strIngredient2;
  final String? strIngredient3;
  final String? strIngredient4;
  final String? strIngredient5;
  final String? strIngredient6;
  final String? strIngredient7;
  final String? strIngredient8;
  final String? strIngredient9;
  final String? strIngredient10;
  final String? strIngredient11;
  final String? strIngredient12;
  final String? strIngredient13;
  final String? strIngredient14;
  final String? strIngredient15;
  final String? strMeasure1;
  final String? strMeasure2;
  final String? strMeasure3;
  final String? strMeasure4;
  final String? strMeasure5;
  final String? strMeasure6;
  final String? strMeasure7;
  final String? strMeasure8;
  final String? strMeasure9;
  final String? strMeasure10;
  final String? strMeasure11;
  final String? strMeasure12;
  final String? strMeasure13;
  final String? strMeasure14;
  final String? strMeasure15;
  final String? strImageSource;
  final String? strImageAttribution;
  final String? strCreativeCommonsConfirmed;
  final String? dateModified;

  @override
  List<Object?> get props => [ idDrink,strDrink,strDrinkAlternate, strTags, strVideo, strCategory, strIBA, strAlcoholic, strGlass, strInstructions, strInstructionsES, strInstructionsDE, strInstructionsFR, strInstructionsIT, strInstructionsZH_HANS, strInstructionsZH_HANT, strDrinkThumb, strIngredient1, strIngredient2, strIngredient3, strIngredient4, strIngredient5, strIngredient6, strIngredient7, strIngredient8, strIngredient9, strIngredient10, strIngredient11, strIngredient12, strIngredient13, strIngredient14, strIngredient15, strMeasure1, strMeasure2, strMeasure3, strMeasure4, strMeasure5, strMeasure6, strMeasure7, strMeasure8, strMeasure9, strMeasure10, strMeasure11, strMeasure12, strMeasure13, strMeasure14, strMeasure15, strImageSource, strImageAttribution, strCreativeCommonsConfirmed, dateModified];

  Map<String, dynamic> toJson() => _$CocktailToJson(this);

  String getStrIngredient(){
    return ((this.strIngredient1 ?? "%")+", "
    + (this.strIngredient2 ?? "%")+", "
    + (this.strIngredient3 ?? "%")+", "
    + (this.strIngredient4 ?? "%")+", "
    + (this.strIngredient5 ?? "%")+", "
    + (this.strIngredient6 ?? "%")+", "
    + (this.strIngredient7 ?? "%")+", "
    + (this.strIngredient8 ?? "%")+", "
    + (this.strIngredient9 ?? "%")+", "
    + (this.strIngredient10 ?? "%")+", "
    + (this.strIngredient11 ?? "%")+", "
    + (this.strIngredient12 ?? "%")+", "
    + (this.strIngredient13 ?? "%")+", "
    + (this.strIngredient14 ?? "%")+", "
    + (this.strIngredient15 ?? "%")).replaceAll(", %", "");
  }

  String getStringIngredient(int index) {
    switch (index) {
      case 1: return strIngredient1 ?? "";
      case 2: return strIngredient2 ?? "";
      case 3: return strIngredient3 ?? "";
      case 4: return strIngredient4 ?? "";
      case 5: return strIngredient5 ?? "";
      case 6: return strIngredient6 ?? "";
      case 7: return strIngredient7 ?? "";
      case 8: return strIngredient8 ?? "";
      case 9: return strIngredient9 ?? "";
      case 10: return strIngredient10 ?? "";
      case 11: return strIngredient11 ?? "";
      case 12: return strIngredient12 ?? "";
      case 13: return strIngredient13 ?? "";
      case 14: return strIngredient14 ?? "";
      case 15: return strIngredient15 ?? "";
      default: return "";
    }
  }

  String getStringMeasure(int index) {
    switch (index) {
      case 1: return strMeasure1 ?? "";
      case 2: return strMeasure2 ?? "";
      case 3: return strMeasure3 ?? "";
      case 4: return strMeasure4 ?? "";
      case 5: return strMeasure5 ?? "";
      case 6: return strMeasure6 ?? "";
      case 7: return strMeasure7 ?? "";
      case 8: return strMeasure8 ?? "";
      case 9: return strMeasure9 ?? "";
      case 10: return strMeasure10 ?? "";
      case 11: return strMeasure11 ?? "";
      case 12: return strMeasure12 ?? "";
      case 13: return strMeasure13 ?? "";
      case 14: return strMeasure14 ?? "";
      case 15: return strMeasure15 ?? "";
      default: return "";
    }
  }

  int ingredientLength(){
    int count = 0;
    for (int i = 1; i <= 15; i++) {
      String ingredient = getStringIngredient(i);
      if (ingredient != null && ingredient.isNotEmpty)
        count++;
    }
    return count;
  }
  
  Cocktail copyWith({
    String? idDrink,
    String? strDrink,
    String? strDrinkAlternate,
    String? strTags,
    String? strVideo,
    String? strCategory,
    String? strIBA,
    String? strAlcoholic,
    String? strGlass,
    String? strInstructions,
    String? strInstructionsES,
    String? strInstructionsDE,
    String? strInstructionsFR,
    String? strInstructionsIT,
    String? strInstructionsZH_HANS,
    String? strInstructionsZH_HANT,
    String? strDrinkThumb,
    String? strIngredient1,
    String? strIngredient2,
    String? strIngredient3,
    String? strIngredient4,
    String? strIngredient5,
    String? strIngredient6,
    String? strIngredient7,
    String? strIngredient8,
    String? strIngredient9,
    String? strIngredient10,
    String? strIngredient11,
    String? strIngredient12,
    String? strIngredient13,
    String? strIngredient14,
    String? strIngredient15,
    String? strMeasure1,
    String? strMeasure2,
    String? strMeasure3,
    String? strMeasure4,
    String? strMeasure5,
    String? strMeasure6,
    String? strMeasure7,
    String? strMeasure8,
    String? strMeasure9,
    String? strMeasure10,
    String? strMeasure11,
    String? strMeasure12,
    String? strMeasure13,
    String? strMeasure14,
    String? strMeasure15,
    String? strImageSource,
    String? strImageAttribution,
    String? strCreativeCommonsConfirmed,
    String? dateModified,
  }) {
    return Cocktail(
      idDrink: idDrink ?? this.idDrink,
      strDrink: strDrink ?? this.strDrink,
      strDrinkAlternate: strDrinkAlternate ?? this.strDrinkAlternate,
      strTags: strTags ?? this.strTags,
      strVideo: strVideo ?? this.strVideo,
      strCategory: strCategory ?? this.strCategory,
      strIBA: strIBA ?? this.strIBA,
      strAlcoholic: strAlcoholic ?? this.strAlcoholic,
      strGlass: strGlass ?? this.strGlass,
      strInstructions: strInstructions ?? this.strInstructions,
      strInstructionsES: strInstructionsES ?? this.strInstructionsES,
      strInstructionsDE: strInstructionsDE ?? this.strInstructionsDE,
      strInstructionsFR: strInstructionsFR ?? this.strInstructionsFR,
      strInstructionsIT: strInstructionsIT ?? this.strInstructionsIT,
      strInstructionsZH_HANS: strInstructionsZH_HANS ?? this.strInstructionsZH_HANS,
      strInstructionsZH_HANT: strInstructionsZH_HANT ?? this.strInstructionsZH_HANT,
      strDrinkThumb: strDrinkThumb ?? this.strDrinkThumb,
      strIngredient1: strIngredient1 ?? this.strIngredient1,
      strIngredient2: strIngredient2 ?? this.strIngredient2,
      strIngredient3: strIngredient3 ?? this.strIngredient3,
      strIngredient4: strIngredient4 ?? this.strIngredient4,
      strIngredient5: strIngredient5 ?? this.strIngredient5,
      strIngredient6: strIngredient6 ?? this.strIngredient6,
      strIngredient7: strIngredient7 ?? this.strIngredient7,
      strIngredient8: strIngredient8 ?? this.strIngredient8,
      strIngredient9: strIngredient9 ?? this.strIngredient9,
      strIngredient10: strIngredient10 ?? this.strIngredient10,
      strIngredient11: strIngredient11 ?? this.strIngredient11,
      strIngredient12: strIngredient12 ?? this.strIngredient12,
      strIngredient13: strIngredient13 ?? this.strIngredient13,
      strIngredient14: strIngredient14 ?? this.strIngredient14,
      strIngredient15: strIngredient15 ?? this.strIngredient15,
      strMeasure1: strMeasure1 ?? this.strMeasure1,
      strMeasure2: strMeasure2 ?? this.strMeasure2,
      strMeasure3: strMeasure3 ?? this.strMeasure3,
      strMeasure4: strMeasure4 ?? this.strMeasure4,
      strMeasure5: strMeasure5 ?? this.strMeasure5,
      strMeasure6: strMeasure6 ?? this.strMeasure6,
      strMeasure7: strMeasure7 ?? this.strMeasure7,
      strMeasure8: strMeasure8 ?? this.strMeasure8,
      strMeasure9: strMeasure9 ?? this.strMeasure9,
      strMeasure10: strMeasure10 ?? this.strMeasure10,
      strMeasure11: strMeasure11 ?? this.strMeasure11,
      strMeasure12: strMeasure12 ?? this.strMeasure12,
      strMeasure13: strMeasure13 ?? this.strMeasure13,
      strMeasure14: strMeasure14 ?? this.strMeasure14,
      strMeasure15: strMeasure15 ?? this.strMeasure15,
      strImageSource: strImageSource ?? this.strImageSource,
      strImageAttribution: strImageAttribution ?? this.strImageAttribution,
      strCreativeCommonsConfirmed: strCreativeCommonsConfirmed ?? this.strCreativeCommonsConfirmed,
      dateModified: dateModified ?? this.dateModified,
    );
  }
}