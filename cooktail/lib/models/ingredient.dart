import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cocktail_repository/cocktail_repository.dart' as ingredient_repository;

part 'ingredient.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Ingredient extends Equatable {
  const Ingredient({
    required this.idIngredient,
    required this.strIngredient,
    required this.strDescription,
    required this.strType,
    required this.strAlcohol,
    required this.strABV,
  });

  factory Ingredient.fromJson(Map<String, dynamic> json) =>
      _$IngredientFromJson(json);

  factory Ingredient.fromRepository(ingredient_repository.Ingredient_Repository Ingredient_Repository) {
    return Ingredient(
      idIngredient : Ingredient_Repository.idIngredient,
      strIngredient : Ingredient_Repository.strIngredient,
      strDescription : Ingredient_Repository.strDescription,
      strType : Ingredient_Repository.strType,
      strAlcohol : Ingredient_Repository.strAlcohol,
      strABV : Ingredient_Repository.strABV,
    );
  }

  static final empty = Ingredient(
    idIngredient: "",
    strIngredient: "",
    strDescription: "",
    strType: "",
    strAlcohol: "",
    strABV: "",
  );

  final String? idIngredient;
  final String? strIngredient;
  final String? strDescription;
  final String? strType;
  final String? strAlcohol;
  final String? strABV;

  @override
  List<Object?> get props => [ idIngredient,strIngredient,strDescription, strType, strAlcohol, strABV];

  Map<String, dynamic> toJson() => _$IngredientToJson(this);


  Ingredient copyWith({
    String? idIngredient,
    String? strIngredient,
    String? strDescription,
    String? strType,
    String? strAlcohol,
    String? strABV,
  }) {
    return Ingredient(
      idIngredient: idIngredient ?? this.idIngredient,
      strIngredient: strIngredient ?? this.strIngredient,
      strDescription: strDescription ?? this.strDescription,
      strType: strType ?? this.strType,
      strAlcohol: strAlcohol ?? this.strAlcohol,
      strABV: strABV ?? this.strABV,
    );
  }
}