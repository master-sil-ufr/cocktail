// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredient.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Ingredient _$IngredientFromJson(Map<String, dynamic> json) => $checkedCreate(
      'Ingredient',
      json,
      ($checkedConvert) {
        final val = Ingredient(
          idIngredient: $checkedConvert('idIngredient', (v) => v as String?),
          strIngredient: $checkedConvert('strIngredient', (v) => v as String?),
          strDescription:
              $checkedConvert('strDescription', (v) => v as String?),
          strType: $checkedConvert('strType', (v) => v as String?),
          strAlcohol: $checkedConvert('strAlcohol', (v) => v as String?),
          strABV: $checkedConvert('strABV', (v) => v as String?),
        );
        return val;
      },
    );

Map<String, dynamic> _$IngredientToJson(Ingredient instance) =>
    <String, dynamic>{
      'idIngredient': instance.idIngredient,
      'strIngredient': instance.strIngredient,
      'strDescription': instance.strDescription,
      'strType': instance.strType,
      'strAlcohol': instance.strAlcohol,
      'strABV': instance.strABV,
    };
