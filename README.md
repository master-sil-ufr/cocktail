
# CookTail

SDK : *Flutter*  
Année Scolaire : *M2 SIL 2023-2024*  
Matière : *Programmation mobile*  
Professeur : *M. PAULUS Lucas*  


## Présentation

Cooktail est une application mobile créée à l'aide de Flutter.  Elle permet à l'utilisateur de visualisé une liste de cocktail mis à disposition sur l'API [Thecocktaildb](https://www.thecocktaildb.com/api.php).


### Fonctionnalités proposées
- Suggestion aléatoire
- Liste par catégorie, ingrédients, type de verre, alcoolisée ou non
- Détail d'un cocktail
- Détail d'un ingrédient
- Recherche avec tri sur le nom, l'ingrédient ou la première lettre
- Gestion des favoris

### Groupe
Filipe AUGUSTO <<faugusto@etu.unistra.fr>>  
Luc BURCKEL <<luc.burckel@etu.unistra.fr>>

## Installation
Télécharger et installer [cooktail.apk](https://git.unistra.fr/filipe-et-luc/cooktail/-/blob/main/cooktail.apk) sur votre téléphone Android 



## Onglets
### Accueil
![Texte alternatif](/img/accueil.png "Accueil").
### Recherche
![Texte alternatif](/img/recherche.png "Recherche").
### Favoris
![Texte alternatif](/img/favoris.png "Favoris").

### Liste par Catégorie 
![Texte alternatif](/img/liste.png "Liste").

### Détail cocktail 
![Texte alternatif](/img/detail.png "Cocktail").

### Détail ingrédient 
![Texte alternatif](/img/ingredient.png "Ingrédient").